import socket

def send_udp_event(message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ('localhost', 5005)
    sock.sendto(message.encode(), server_address)
    sock.close()

# Send "RECORDING" to toggle recording
send_udp_event("RECORDING")
