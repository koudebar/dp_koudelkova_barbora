# -----------------------------------------
# Project Name: MABC-2 Pose Classification
# Name: Nicholas Renotte, Barbora Koudelkova
# Date: January 2020
# Description: This code trains custom model from xyz acquired coordinates.
#              The script is inspired and uses scikit data annotation part written by Nicholas Renotte at (https://github.com/nicknochnack/Body-Language-Decoder)
# -----------------------------------------

# Read and split data
import pandas as pd
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import accuracy_score, classification_report  # Accuracy metrics
import pickle
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier

df = pd.read_csv('coordsOriginal.csv')
print(df.head())
print(df.tail())

# Step 1: Identify columns that are completely NaN
all_nan_columns = df.columns[df.isna().all()]

df_cleaned = df.dropna(axis=1, how='all')  # dropping completely NaN columns

# Calculate the indices for x24 to v33, offset by one because the first column is 'class' - i only want to detect points from hip below(23-32), since the detection is
# not precise at all, all points that has nothing to do with it (upper torso), i do not use
start_index = 1 + 4 * (24 - 1)  # 'class' being the first column
end_index = 1 + 4 * 33 # end index for v33, includes the offset for 'class'

# Select columns from x23 to v32 plus the 'class' column
columns_to_keep = ['class'] + df_cleaned.columns[start_index:end_index].tolist()
df_reduced = df_cleaned[columns_to_keep]

print("DF REDUCED")
print(df_reduced)

# keep only the body pose estimators // 4 * 33 points + target value, because hand detection causes Clever Hans effect.
df_cleaned = df_cleaned.iloc[:, :133]

X = df_reduced.drop('class', axis=1) # features
y = df_reduced['class'] # target value

print(y.value_counts())

# Scale features, due to class imbalance
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)
smote = SMOTE(random_state=42)
X_res, y_res = smote.fit_resample(X_scaled, y)
# Check the balance of classes after SMOTE
print(y_res.value_counts())

#X_train, X_test, y_train, y_test = train_test_split(X_res, y_res, test_size=0.3, random_state=42, stratify=y_res) #70/30 split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42, stratify=y) #70/30 split

# Train ML model

pipelines = {
    'lr':make_pipeline(StandardScaler(), LogisticRegression(class_weight='balanced')),
    'rc':make_pipeline(StandardScaler(), RidgeClassifier()),
    'rf':make_pipeline(StandardScaler(), RandomForestClassifier(class_weight='balanced')),
    'gb':make_pipeline(StandardScaler(), GradientBoostingClassifier()),
}
fit_models = {}
for algo, pipeline in pipelines.items():
    model = pipeline.fit(X_train, y_train)
    fit_models[algo] = model
fit_models
#fit_models['rc'].predict(X_test)

#Evaluate Model


for algo, model in fit_models.items():
    yhat = model.predict(X_test)
    print(algo, accuracy_score(y_test, yhat))
fit_models['rf'].predict(X_test)
y_test
with open('body_language.pkl', 'wb') as f:
    pickle.dump(fit_models['rf'], f)

# Initialize the Random Forest classifier
# Here you can configure your RandomForest with desired parameters
random_forest_model = RandomForestClassifier(class_weight='balanced', n_estimators=100, random_state=42)

# Create a pipeline that includes scaling and classification
pipeline = make_pipeline(StandardScaler(), random_forest_model)

# Initialize k-fold cross-validation
k_fold = KFold(n_splits=10, shuffle=True, random_state=42)

# Perform cross-validation
cv_scores = cross_val_score(pipeline, X, y, cv=k_fold, scoring='accuracy')

# Output the mean and standard deviation of the cross-validation scores
print("CV Scores: ", cv_scores)
print("CV Mean: ", cv_scores.mean())
print("CV Standard deviation: ", cv_scores.std())

# Load unseen data
unseen_data = pd.read_csv('unseen_data.csv')

unseen_cleaned = unseen_data.dropna(axis=1, how='all')  # dropping completely NaN columns
# Select columns from x23 to v32 plus the 'class' column
unseen_columns_to_keep = ['class'] + unseen_cleaned.columns[start_index:end_index].tolist()
unseen_data_ready = unseen_cleaned[unseen_columns_to_keep]
X_unseen = unseen_data_ready.drop('class', axis=1)  # Features
y_unseen = unseen_data_ready['class']  # Labels

# Load the trained model
with open('body_language.pkl', 'rb') as file:
    trained_model = pickle.load(file)

# If you used a StandardScaler or any other transformation, apply it here too
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_unseen_scaled = scaler.fit_transform(X_unseen)
# Make predictions
y_pred_unseen = trained_model.predict(X_unseen_scaled)

# Calculate accuracy or other metrics
accuracy_unseen = accuracy_score(y_unseen, y_pred_unseen)
print("Accuracy on unseen data: ", accuracy_unseen)

# More detailed performance analysis
print(classification_report(y_unseen, y_pred_unseen))
