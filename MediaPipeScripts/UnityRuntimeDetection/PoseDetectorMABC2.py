# -----------------------------------------
# Name: Bc.Barbora Koudelkova
# School: Czech Technical University in Prague
# Date: May 2024
# Description: This code uses mediapipe to detect assessments from MABC-2.
#              It uses real-time angle calculations and fitted ML pickle models.
#              It also records the skeletal data to csv when recording flag is raised from Unity application
#              It listens via localhost on port 5005 and sends detected poses on port 1234
# -----------------------------------------
import csv
import threading
from datetime import datetime

# Import necessary libraries
import numpy as np
import pandas as pd
import pickle
import warnings
import mediapipe as mp
import cv2
import time
import socket
import sys

mp_drawing = mp.solutions.drawing_utils  # Drawing helpers
mp_holistic = mp.solutions.holistic  # Mediapipe Solutions
mp_pose = mp.solutions.pose #pose connectionsq


# Initialize socket for UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('localhost', 1234)  # Set to the appropriate IP address and port
start_time = None
standing = False
threshold = 0.04 #0.05

# Define the variables for recording the session
time_string = datetime.now().strftime("%Y-%m-%d_%H_%M")
csv_file = f'recording_coordinates_{time_string}.csv'
# UDP trigger flags and lock
recording = False
recording_lock = threading.Lock()
scene_name = ""
scene_name_lock = threading.Lock()
detected_pose = ""

# Function to listen for UDP messages and toggle recording
def udp_listener():
    global recording, scene_name
    udp_ip = "0.0.0.0"
    udp_port = 5005

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((udp_ip, udp_port))

    num_coords = 33  #pose xyz and visibility coord 33*4
    landmarks = ['timestamp', 'scene', 'pose_detected']
    for val in range(1, num_coords+1):
        landmarks += ['x{}'.format(val), 'y{}'.format(val), 'z{}'.format(val), 'v{}'.format(val)]
    with open(csv_file, mode='w', newline='') as f:
        csv_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(landmarks)

    try:
        while True:
            data, address = sock.recvfrom(4096)
            message = data.decode().lower()
            if message == "recording":
                with recording_lock:
                    recording = not recording
                    status = "Started" if recording else "Stopped"
                print(f"Recording {status}")
            else:
                with scene_name_lock:
                    scene_name = message
                print(f"Scene name set to {scene_name}")
    except OSError as e:
        print(f"UDP Listener Error: {e}")

# Start the UDP listener in a separate thread
listener_thread = threading.Thread(target=udp_listener, daemon=True)
listener_thread.start()

# Function to save pose landmarks to a CSV file
def save_pose_landmarks_to_csv(landmarks, file_path, detected_pose):
    try:
        with scene_name_lock:
            print(scene_name)
            current_scene_name = scene_name
        with open(file_path, mode='a', newline='') as file:
            writer = csv.writer(file)
            row = [datetime.now().strftime("%Y-%m-%d %H:%M:%S"), current_scene_name, detected_pose]
            for landmark in landmarks:
                row.extend([landmark.x, landmark.y, landmark.z, landmark.visibility])
            writer.writerow(row)
            #print(f"Data saved to {file_path}: {row}")
    except Exception as e:
        print(f"Error writing to CSV file: {e}")


# Suppress warning that X does not have valid feature names, but StandardScaler was fitted with feature names, raised in the code
warnings.filterwarnings('ignore', category=UserWarning)

#load ML model
with open('body_language.pkl', 'rb') as f:
    model = pickle.load(f)

cap = cv2.VideoCapture(0)

# Initiate holistic model
with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
    while cap.isOpened():
        ret, frame = cap.read()
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False

        results = holistic.process(image)

        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # Draw face landmarks
        mp_drawing.draw_landmarks(
            image, results.face_landmarks, mp_holistic.FACEMESH_TESSELATION,
            mp_drawing.DrawingSpec(color=(80, 110, 10), thickness=1, circle_radius=1),
            mp_drawing.DrawingSpec(color=(80, 256, 121), thickness=1, circle_radius=1)
        )

        # Draw right hand
        mp_drawing.draw_landmarks(
            image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS,
            mp_drawing.DrawingSpec(color=(80, 22, 10), thickness=2, circle_radius=4),
            mp_drawing.DrawingSpec(color=(80, 44, 121), thickness=2, circle_radius=2)
        )

        # Draw left hand
        mp_drawing.draw_landmarks(
            image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS,
            mp_drawing.DrawingSpec(color=(121, 22, 76), thickness=2, circle_radius=4),
            mp_drawing.DrawingSpec(color=(121, 44, 250), thickness=2, circle_radius=2)
        )

        # Draw pose
        mp_drawing.draw_landmarks(
            image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS,
            mp_drawing.DrawingSpec(color=(245, 117, 66), thickness=2, circle_radius=4),
            mp_drawing.DrawingSpec(color=(245, 66, 230), thickness=2, circle_radius=2)
        )

        # Export coordinates
        try:
            # Extract pose landmarks
            pose = results.pose_landmarks.landmark
            pose_row = list(np.array([[landmark.x, landmark.y, landmark.z, landmark.visibility] for landmark in pose]).flatten())

            # extract only the coordinates from hips below
            # Select only the landmarks from 24th to 33rd (zero-indexed 23 to 32)
            selected_landmarks = pose[23:33]  # Adjust indices accordingly

            # Rewrite a list containing only the selected landmarks' data
            pose_row = list(np.array([[landmark.x, landmark.y, landmark.z, landmark.visibility]
                                      for landmark in selected_landmarks]).flatten())



            left_ankle = pose[mp_holistic.PoseLandmark.LEFT_ANKLE.value]
            right_ankle = pose[mp_holistic.PoseLandmark.RIGHT_ANKLE.value]
            # Calculate the difference in y-coordinates between left and right ankles
            y_diff = abs(left_ankle.y - right_ankle.y)

            # Prepare for prediction
            X = pd.DataFrame([pose_row])
            body_language_class = model.predict(X)[0]
            body_language_prob = model.predict_proba(X)[0]

            max_prob = round(body_language_prob[np.argmax(body_language_prob)], 2)

            if right_ankle.visibility > 0.75 and left_ankle.visibility > 0.75 and not standing:
                cv2.putText(image, "START!", (50, 200), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
                if start_time is None:
                    start_time = time.time()
                message = "Start"
                detected_pose = "None"
                sock.sendto(message.encode(), server_address)

            # Display only if probability is greater than 0.7
            if max_prob > 0.45:
                print(body_language_class, body_language_prob)
                # Get status box
                cv2.rectangle(image, (0, 0), (250, 60), (245, 117, 16), -1)
                # Display Class and Probability
                cv2.putText(image, f'CLASS: {body_language_class}', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f'PROB: {max_prob}', (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA)

            one_leg_conditions = (y_diff >= threshold and (right_ankle.visibility > 0.5 and left_ankle.visibility > 0.5)) or (max_prob > 0.45 and (right_ankle.visibility <= 0.5 or left_ankle.visibility <= 0.5))
            if one_leg_conditions:
                standing = True
                foot_text = "RightFoot" if left_ankle.y < right_ankle.y or (body_language_class == "LegStandCorrectRight" or body_language_class == "LegStandIncorrectRight") else "Left Foot"
                message = f"Player is standing on one leg, Foot: {foot_text}"
                detected_pose = body_language_class
                # Send UDP message
                sock.sendto(message.encode(), server_address)
                cv2.putText(image, f"Standing on one leg! Y-diff: {y_diff}", (50, 100), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 0, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f"Foot: {foot_text}", (500, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2,
                            cv2.LINE_AA)
            else:
                # Person is not standing on one leg, reset the counting
                standing = False
                start_time = None
                detected_pose = None
            # Save the world coordinates if recording is enabled
            with recording_lock:
                if recording:
                    save_pose_landmarks_to_csv(results.pose_world_landmarks.landmark, csv_file, detected_pose)

        except AttributeError:
            pass

        cv2.imshow('Kidnap Capture', image)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

# Release the webcam and close all windows
cap.release()
cv2.destroyAllWindows()

#Draw the last frame
#mp_drawing.plot_landmarks(results.pose_world_landmarks, mp_pose.POSE_CONNECTIONS)
#print(results.pose_world_landmarks)
