# -----------------------------------------
# Name: Bc.Barbora Koudelkova
# School: Czech Technical University in Prague
# Date: December 2023
# Description: This code uses mediapipe to detect bad postural habits. It only measures the left side of the body.
# -----------------------------------------


# Import necessary libraries
import mediapipe as mp
import cv2
import math
import numpy as np

# Initialize MediaPipe drawing and pose modules
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

# Open the webcam (0 represents the default camera)
cap = cv2.VideoCapture(0)

# Define a function to calculate the angle between three points
def calculate_angle(a, b, c):
    a = np.array(a)  # First point
    b = np.array(b)  # Mid point
    c = np.array(c)  # End point

    # Calculate the angle between the three points
    radians = np.arctan2(c[1] - b[1], c[0] - b[0]) - np.arctan2(a[1] - b[1], a[0] - b[0])
    angle = np.abs(radians * 180.0 / np.pi)

    # Ensure the angle is within 0 to 180 degrees
    if angle > 180.0:
        angle = 360 - angle

    return angle

# Initialize the MediaPipe Pose model with confidence thresholds
with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as pose:
    while cap.isOpened():
        # Read a frame from the webcam
        ret, frame = cap.read()

        # Convert the frame to RGB color format
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False

        # Process the frame using the Pose model
        results = pose.process(image)

        # Make the image writable again and convert it back to BGR format
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        try:
            # Extract pose landmarks
            landmarks = results.pose_landmarks.landmark

            # Get the height and width of the frame
            height, width, _ = image.shape

            # Get the coordinates of the left shoulder and left hip landmarks
            shoulder = [landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x * width,
                        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y * height]

            pelvic = [landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x * width,
                      landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y * height]

            # Calculate angles using the custom function
            angle_pelvic = calculate_angle([0, pelvic[1]], pelvic, shoulder)
            angle_shoulder = calculate_angle([0, shoulder[1]], shoulder, pelvic)

            # Draw horizontal lines corresponding to pelvic and shoulder landmarks
            cv2.line(image, (0, int(pelvic[1])), (width, int(pelvic[1])), (0, 255, 0), 2)
            cv2.line(image, (0, int(shoulder[1])), (width, int(shoulder[1])), (0, 255, 0), 2)

            # Visualize angle at the pelvic and shoulder positions
            cv2.putText(image, f"Pelvic Angle: {angle_pelvic:.2f} degrees", (int(pelvic[0]), int(pelvic[1])),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
            cv2.putText(image, f"Shoulder Angle: {angle_shoulder:.2f} degrees", (int(shoulder[0]), int(shoulder[1])),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)

            # Draw vertical lines connecting the pelvic point to the shoulder line and vice versa
            cv2.line(image, (int(pelvic[0]), int(pelvic[1])), (int(shoulder[0]), int(pelvic[1])), (0, 0, 255), 2)
            cv2.line(image, (int(pelvic[0]), int(shoulder[1])), (int(shoulder[0]), int(shoulder[1])), (0, 0, 225), 2)

            # Draw horizontal lines connecting the pelvic point to the shoulder line and vice versa
            cv2.line(image, (int(pelvic[0]), int(pelvic[1])), (int(pelvic[0]), int(shoulder[1])), (255, 0, 0), 2)
            cv2.line(image, (int(shoulder[0]), int(shoulder[1])), (int(shoulder[0]), int(pelvic[1])), (255, 0, 0), 2)

            # Draw the pelvic and shoulder points
            cv2.circle(image, (int(pelvic[0]), int(pelvic[1])), 12, (245, 117, 66), -1)
            cv2.circle(image, (int(shoulder[0]), int(shoulder[1])), 12, (245, 117, 66), -1)

        except Exception as e:
            print(e)

        # Display the annotated frame with angles
        cv2.imshow('PelvicShoulderDifference', image)

        # Press 'q' to exit the application
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

# Release the webcam and close all windows
cap.release()
cv2.destroyAllWindows()
