# -----------------------------------------
# Name: Bc.Barbora Koudelkova
# School: Czech Technical University in Prague
# Date: December 2023
# Description: This code uses mediapipe to detect BAL1 assessment from MABC-2
# -----------------------------------------

# Import necessary libraries
import mediapipe as mp
import cv2
import time

# Initialize MediaPipe drawing and pose modules
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

# Open the webcam (0 represents the default camera)
cap = cv2.VideoCapture(0)
counter = 0
threshold = 0.05
start_time = None
standing = False
previous_hip_y = None

# Initialize the MediaPipe Pose model with confidence thresholds
with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as pose:
    while cap.isOpened():
        # Read a frame from the webcam
        ret, frame = cap.read()

        # Convert the frame to RGB color format
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False

        # Process the frame using the Pose model
        results = pose.process(image)

        # Make the image writable again and convert it back to BGR format
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # Extract pose landmarks
        try:
            landmarks = results.pose_landmarks.landmark

            left_ankle = landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value]
            right_ankle = landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value]

            left_shoulder = landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value]
            right_shoulder = landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value]

            left_hand = landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value]
            right_hand = landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value]

            left_elbow = landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value]
            right_elbow = landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value]

            hips = landmarks[mp_pose.PoseLandmark.LEFT_HIP.value]

            # Calculate the difference in y-coordinates between left and right ankles
            y_diff = abs(left_ankle.y - right_ankle.y)
            shoulder_width = abs(left_shoulder.x - right_shoulder.x)

            # Calculate horizontal positions of hands
            left_hand_x = left_hand.x if left_hand.visibility > 0 else -1
            right_hand_x = right_hand.x if right_hand.visibility > 0 else -1

            left_elbow_y = left_elbow.y if left_elbow.visibility > 0 else -1
            right_elbow_y = right_elbow.y if right_elbow.visibility > 0 else -1

            # Example heuristics - adjust these based on observation and experimentation
            current_hip_y = hips.y
            stiff_body = shoulder_width < 0.1 and abs(left_hand_x - right_hand_x) < 0.1  # Adjust thresholds as needed
            hip_movement = abs(current_hip_y - previous_hip_y) if previous_hip_y is not None else 0


            exaggerated_arm_movements = abs(left_shoulder.y - right_shoulder.y) > 0.01 and abs(left_elbow_y - right_elbow_y) > 0.05  # Adjust threshold as needed
            # Calculate the midpoint between left and right ankles
            midpoint_x = (left_ankle.x + right_ankle.x) / 2
            midpoint_y = (left_ankle.y + right_ankle.y) / 2

            print(left_elbow_y, right_elbow_y)
            print(left_shoulder, right_shoulder)

            # Example threshold for swaying violently
            swaying_violently = abs(midpoint_x - 0.5) > 0.05

            flabby_body = not stiff_body and exaggerated_arm_movements and swaying_violently

            if right_ankle.visibility > 0.7 and left_ankle.visibility > 0.7 and not standing:
                cv2.putText(image, "START!", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
                if start_time is None:
                    start_time = time.time()

            # Check if both ankles are above a certain y-coordinate threshold
            if y_diff > threshold and (right_ankle.visibility > 0.7 and left_ankle.visibility > 0.7):
                standing = True
                elapsed_time = time.time() - start_time if start_time is not None else 0

                # Determine which foot the person is standing on
                foot_text = "Left Foot" if left_ankle.y < right_ankle.y else "Right Foot"

                cv2.putText(image, f"Stiff Body: {stiff_body}", (50, 200), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255),
                            2, cv2.LINE_AA)
                cv2.putText(image, f"Flabby Body: {flabby_body}", (50, 250), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f"Exaggerated Arm Movements: {exaggerated_arm_movements}", (50, 300),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f"Swaying Violently: {swaying_violently}", (50, 400), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (255, 255, 255), 2, cv2.LINE_AA)

                cv2.putText(image, f"Standing on one leg! Y-diff: {y_diff}", (50, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f"Elapsed Time: {elapsed_time:.2f} seconds", (50, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
                cv2.putText(image, f"Foot: {foot_text}", (500, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
            else:
                # Person is not standing on one leg, reset the counting
                standing = False
                start_time = None
        except:
            pass
        #visualise
        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                  mp_drawing.DrawingSpec(color=(245, 117, 66), thickness=2, circle_radius=2),
                                  mp_drawing.DrawingSpec(color=(245, 66, 230), thickness=1, circle_radius=2)
                                  )
        cv2.imshow('Kidnap Capture', image)

        #close the app
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

# Release the webcam and close all windows
cap.release()
cv2.destroyAllWindows()
