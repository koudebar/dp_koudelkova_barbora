# DP_Koudelkova_Barbora

This is a repository created for Master´s thesis 2024 and semestral project.

**Interactive application using motion data for the analysis of an individual's musculoskeletal system**

*This thesis explores integrating markerless motion capture methods and gamification principles in pediatric physical and occupational therapy. The research aims to simplify the process of testing and evaluating the movement of test subjects to increase engagement and incorporate these methods into standardized medical procedures.

The development involved testing various motion capture methods, emphasizing markerless capture methods using advanced computer vision technologies such as MediaPipe. This approach was complemented by developing a visual projection application that implements a portion of the MABC-2 test, which serves as an intervention method used in occupational therapy.

Following a user-centered design methodology, two rounds of testing were conducted to refine and iteratively improve the proposed application.

The results of testing with users indicate that the system significantly improves interaction with patients and substantially increases the effectiveness of therapeutic exercise. *

# Repository Structure

- **MABC2/** - Unity application
- **MediaPipeScripts/** - MediaPipe scripts
- **Miscellaneous/** - Forms, Score Tabels, Automated documents, recording coordinates
- **CTU_DP_Thesis-Koudelkova.pdf** - my beutiful thesis.


# How to run the aplication

## Unity Application Setup

- Ensure you have Unity version 2023.2.2f1 installed on your system.
- Clone the project repository from GitHub:
  - `git clone https://gitlab.fel.cvut.cz/koudebar/dp_koudelkova_barbora.git`
- Navigate to the folder `MABC2`.
- Open the cloned project in Unity by selecting ‘Open’ from the Unity Hub and navigating to the project directory.
- To run the application within Unity, click the ‘Play’ button in the Unity editor.
- To run the application standalone:
  - Navigate to the folder `MABC2/Build`.
  - Run `mabc2.exe`

## Pharus Software Configuration

- Install the Pharus software if not already installed.
- Configure and set up the sensors and space OR
- Navigate to the folder `Miscellaneous/PharusRecordings`, open terminal in folder, where Pharus.exe is, and type:
  - `./pharus.exe "NAMEOFSCENE".rec`
- Configure the Pharus software to connect with the Unity application:
  - Please ensure the software is set and enabled to send data to the correct IP address and port expected by the DeepSpace application.
  - Do not close Pharus or terminal during Unity runtime

## Installing and Running the Python Scripts

- Prerequisites:
  - Python 3.6 or higher
  - Libraries: MediaPipe, OpenCV, NumPy, scikit-learn, pickle
  - Install the libraries using pip:
    - `pip install mediapipe opencv-python numpy scikit-learn`
- Clone the Python scripts from the repository:
  - `git clone https://gitlab.fel.cvut.cz/koudebar/dp_koudelkova_barbora.git`
- Navigate to the script directory:
  - `cd MediaPipescripts/UnityRuntimeDetection`
- Run the desired script:
  - `python PoseDetectorMABC2.py` - to start real-time pose detection and send data to Unity. This MUST be launched during runtime.
  - `python PoseClassificatorMABC2.py` - to collect data for model training.
  - `python TrainPoseClassificator.py` - to train or retrain the model.

## Setup

- To calibrate space with the application correctly, in Unity Editor, set the prefab "TrackingReceiveHandler" to dimensions identical to the Pharus space setup and the desired resolution that will be projected.
- If part of the tracking area in Pharus is set to obstructed and the projection area is partial, change the "Canvas" prefab location to the corresponding coordinates of the projection area.

## Troubleshooting

- Ensure all components (Unity, Pharus, Python) are correctly installed and configured.
- Check for software updates regularly to ensure compatibility and performance.
- For connectivity issues with Pharus software, verify network settings and server logs.
- If Python scripts fail to run, ensure all dependencies are installed, and the Python version is correct.



# Posture Analysis Python Script Instructions

This document provides instructions on how to set up and run the posture analysis Python script.

## Prerequisites

- Python 3.6 or higher
- MediaPipe
- OpenCV
- NumPy

## Installation

- Ensure Python is installed on your system.
- Install the required libraries using pip: pip install mediapipe opencv-python numpy


## Running the Script

- Download the posture analysis scripts from this GitLab repository

- Navigate to the directory where the script is located: `MediaPipeScripts/Prototypes`
- Run the scripts using the following command:
- `python DiafragmXPelvicFloor.py` for running posture prototype 1
- `python AnkleDiff.py` for running posture prototype 2

- videos demonstrating the functionality are available in the same directory

## Using the Application

For the Prototype Number One:

- Stand in front of the camera at a suitable distance to ensure your full upper body is visible.
- The script will display a window showing your live camera feed with posture analysis.
- Stand on your left side facing the camera, perform the posture as required by the assessment.
- The angles at the shoulder and pelvic regions will be calculated and displayed.
- To exit the application, press 'q' on your keyboard.

For the Prototype Number Two:

- Stand in front of the camera at a suitable distance to ensure your full upper body is visible.
- The script will display a window showing your live camera feed with posture analysis and indicating START.
- Perform the posture as required by the MABC-2 BAL1 assessment.
- The BAL1 criteria will be calculated and displayed.
- To exit the application, press 'q' on your keyboard.

## Troubleshooting

If you encounter any issues, ensure that:

- The camera is properly connected and configured on your computer.
- All the required Python libraries are installed.
- You have the latest version of the script from the GitLab repository.

xx
