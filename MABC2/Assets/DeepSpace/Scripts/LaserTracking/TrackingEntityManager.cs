﻿/*-----------------------------------------
 * Original Author: ArsElectronicaFutureLab
 * Source: https://github.com/ArsElectronicaFuturelab/DeepSpaceDevKit
 * Modified by: Bc. Barbora Koudelkova
 * School: Czech Technical University in Prague
 * Date of Modification: March 2024
 * File: TrackingEntityManager.cs
 * Brief: manages tracking entities within a Unity application, handling the instantiation and updates of visual elements based on motion capture data.
 * It registers and updates tracking data, manages visualization settings, and exports data after completion of tracking events.
-----------------------------------------
*/

// Import necessary Unity modules and standard collections
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;

// Define the namespace to organize code related to DeepSpace laser tracking
namespace DeepSpace.LaserTracking
{
    // Define the class TrackingEntityManager, inheriting from MonoBehaviour to interact with Unity's lifecycle and implementing ITrackingReceiver to handle tracking events
	public class TrackingEntityManager : MonoBehaviour, ITrackingReceiver
	{
        // SerializeField allows _trackingReceiveHandler to be set in the Unity Editor, expecting an object that handles incoming tracking data
		[SerializeField]
		private TrackingReceiveHandler _trackingReceiveHandler = null;

        // Dictionary to hold TrackingEntity objects, indexed by an integer ID, to efficiently manage and access them
		protected Dictionary<int, TrackingEntity> _trackingEntityDict = new Dictionary<int, TrackingEntity>();

        // Public Transform for setting the parent of spawned tracking entities in the Unity Editor. It can be null if no parent is desired
		[Tooltip("This transform can be null, if no parent is wanted.")]
		public Transform trackSpawnParent = null;

        // Public GameObject to be set in the Unity Editor, defining the prefab to spawn for each tracking entity
		[Tooltip("This prefab will be spawned for each track.")]
		public GameObject TrackingEntityPrefab = null;
		public GameObject TrackingEntityPrefabNull = null;
		
		[Tooltip("This prefab will be spawned for each echo.")]
		public GameObject EchoPrefab = null;
		public GameObject EchoPrefabNull = null;

        // Determines whether to add a track to the manager when it's updated but not known yet
		public bool addUnknownTrackOnUpdate = true;

        // Offset to be applied to the tracking entity's grid position, can be modified in the Unity Editor
		public Vector2 gridOffset = new Vector2(0.0f, 0.0f);

        // Provides a list of all TrackingEntity objects being managed, encapsulating the dictionary's values for easier access
		public List<TrackingEntity> TrackingEntityList
		{
			get { return new List<TrackingEntity>(_trackingEntityDict.Values); }
		}
		
		public bool debugEchoes = false;
		public bool visualiseTrackEntitites = false;

        // Unity lifecycle method called when the object becomes enabled and active, registering this object as a tracking receiver if the handler is not null
		void OnEnable()
		{
			if (_trackingReceiveHandler != null)
			{
				_trackingReceiveHandler.RegisterTrackingReceiver(this);
			}
		}

        // Unity lifecycle method called when the object becomes disabled or inactive, unregistering this object as a tracking receiver if the handler is not null
		void OnDisable()
		{
			if (_trackingReceiveHandler != null)
			{
				_trackingReceiveHandler.UnregisterTrackingReceiver(this);
			}
		}

        // Event handlers for tracking link events, defined in the ITrackingReceiver interface

        // Handles the event of a new track being detected
		public void OnTrackNew(TrackRecord track)
		{
			TrackAdded(track);
		}

        // Handles the event of an existing track being updated
		public void OnTrackUpdate(TrackRecord track)
		{
			TrackUpdated(track);
		}

        // Handles the event of a track being lost
		public void OnTrackLost(TrackRecord track)
		{
			TrackRemoved(track.trackID);
		}

        // Methods for tracking entity management

        // Adds a new tracking entity for a given track record
		public virtual void TrackAdded(TrackRecord trackRecord)
		{
			Vector2 position = _trackingReceiveHandler.TrackingSettings.GetScreenPositionFromRelativePosition(trackRecord.relPos.x, trackRecord.relPos.y);
			GameObject trackInstance;
			if (visualiseTrackEntitites)
			{
				 trackInstance = GameObject.Instantiate(TrackingEntityPrefab, new Vector3(position.x, position.y, 0), Quaternion.identity) as GameObject;
			}
			else
			{
				 trackInstance = GameObject.Instantiate(TrackingEntityPrefabNull, new Vector3(position.x, position.y, 0), Quaternion.identity) as GameObject;
			}

			trackInstance.transform.SetParent(trackSpawnParent);
			trackInstance.name = string.Format("PharusTrack_{0}", trackRecord.trackID);

			TrackingEntity trackingEntity = trackInstance.GetComponent<TrackingEntity>();
			trackingEntity.TrackID = trackRecord.trackID;

			ApplyTrackData(trackingEntity, trackRecord);

			_trackingEntityDict.Add(trackingEntity.TrackID, trackingEntity);
		}

        // Updates an existing tracking entity with new data from a track record or adds it if it's unknown and `addUnknownTrackOnUpdate` is true
        public virtual void TrackUpdated(TrackRecord trackRecord)
        {

	        // Before applying track data
	        if(debugEchoes)Debug.Log($"Updating TrackID={trackRecord.trackID} with {trackRecord.echoes.Count} Echo(es)");
	        {
		        TrackingEntity trackingEntity = null;
		        if (_trackingEntityDict.TryGetValue(trackRecord.trackID, out trackingEntity))
		        {
			        ApplyTrackData(trackingEntity, trackRecord);

			        trackingEntity.SetPosition(
				        _trackingReceiveHandler.TrackingSettings.GetScreenPositionFromRelativePosition(
					        trackRecord.relPos.x, trackRecord.relPos.y));
		        }
		        else
		        {
			        if (addUnknownTrackOnUpdate)
			        {
				        TrackAdded(trackRecord);
			        }
		        }
	        }
        }

        // Removes a tracking entity based on its track ID
		public virtual void TrackRemoved(int trackID)
		{
			TrackingEntity trackingEntity = null;
			if (_trackingEntityDict.TryGetValue(trackID, out trackingEntity))
			{
				_trackingEntityDict.Remove(trackID);

				if(trackingEntity != null)
				{
					Destroy(trackingEntity.gameObject);
				}
			}
		}

        // Applies data from a TrackRecord to a TrackingEntity, setting positions, orientation, speed, and echoes
		protected virtual void ApplyTrackData(TrackingEntity trackingEntity, TrackRecord trackRecord)
		{
			trackingEntity.AbsolutePosition = new Vector2(trackRecord.currentPos.x - gridOffset.x, trackRecord.currentPos.y - gridOffset.y);
			trackingEntity.NextExpectedAbsolutePosition = new Vector2(trackRecord.expectPos.x - gridOffset.x, trackRecord.expectPos.y - gridOffset.y);
			trackingEntity.RelativePosition = new Vector2(trackRecord.relPos.x, trackRecord.relPos.y);
			trackingEntity.Orientation = new Vector2(trackRecord.orientation.x, trackRecord.orientation.y);
			trackingEntity.Speed = trackRecord.speed;
			
			trackingEntity.Echoes.Clear();
			trackingEntity.EchoScreenPosition.Clear();
			trackingEntity.ClearEchoes(); //clears GameObject echoes
			trackRecord.echoes.AddRange(trackingEntity.Echoes);
			/*Changes made by me*/
			foreach (var echo in trackRecord.echoes)
			{
				Vector2 echoPosition = _trackingReceiveHandler.TrackingSettings.GetScreenPositionFromRelativePosition(echo.x, echo.y);
				GameObject echoInstance;
				if (visualiseTrackEntitites)
				{
					echoInstance = Instantiate(EchoPrefab, new Vector3(echoPosition.x, echoPosition.y, 0),
						Quaternion.identity) as GameObject;
				}
				else
				{
					echoInstance = GameObject.Instantiate(EchoPrefabNull, new Vector3(echoPosition.x, echoPosition.y, 0),Quaternion.identity) as GameObject;
				}
				echoInstance.transform.SetParent(trackSpawnParent);
				trackingEntity.Echoes.Add(new Vector2(echo.x, echo.y));
				trackingEntity.EchoScreenPosition.Add(_trackingReceiveHandler.TrackingSettings.GetScreenPositionFromRelativePosition(echo.x, echo.y));
				trackingEntity.AddEcho(echoInstance); // AddEcho is a method in TrackingEntity to keep track of the echo GameObjects
				if(debugEchoes)Debug.Log($"Added echo to Entity ID={trackingEntity.TrackID}: {echo.x}, {echo.y}");
			}
			/**/
		}
	}
}
