/*-----------------------------------------
 * Original Author: ArsElectronicaFutureLab
 * Source: https://github.com/ArsElectronicaFuturelab/DeepSpaceDevKit
 * Modified by: Bc. Barbora Koudelkova
 * School: Czech Technical University in Prague
 * Date of Modification: March 2024
 * File: TrackingEntity.cs
 * Brief: Models a tracking entity for a laser tracking system, encapsulating all related tracking details such as track ID, position, orientation, and speed.
 * It allows for the management of echoes related to the tracking entity, including adding, displaying, and clearing echoes
-----------------------------------------
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using NUnit.Framework;

namespace DeepSpace.LaserTracking
{
	/// <summary>
	/// Derive your tracking driven entities from this.
	/// </summary>
	public class TrackingEntity : MonoBehaviour
	{
		private int _trackID;
		private Vector2 _absolutePosition;
		private Vector2 _nextExpectedAbsolutePosition;
		private Vector2 _relativePosition;
		private Vector2 _orientation;
		private float _speed;
		private List<Vector2> _echoes = new List<Vector2>();
		[CanBeNull] private List<Vector2> _screenPositionEcho = new List<Vector2>();
		
		// List to store echo GameObjects
		private List<GameObject> echoGameObjects = new List<GameObject>();

		// Method to add an echo GameObject to the tracking entity
		public void AddEcho(GameObject echoInstance)
		{
			echoGameObjects.Add(echoInstance);
		}

		// Method to clear all echo GameObjects
		public void ClearEchoes()
		{
			foreach (GameObject echoGO in echoGameObjects)
			{
				if (echoGO != null)
				{
					Destroy(echoGO); // Destroy the GameObject in Unity
				}
			}
			echoGameObjects.Clear(); // Clear the list
		}



		/// <summary>
		/// TrackID corresponds to the entityId of the tracking service (TUIO sessionID / Pharus trackID).
		/// </summary>
		public int TrackID
		{
			get { return _trackID; }
			set { _trackID = value; }
		}

		/// <summary>
		/// The track's current position in meters (Pharus only)
		/// </summary>
		public Vector2 AbsolutePosition
		{
			get { return _absolutePosition; }
			set { _absolutePosition = value; }
		}

		/// <summary>
		/// The position the track will be expected in the next frame (Pharus only)
		/// </summary>
		public Vector2 NextExpectedAbsolutePosition
		{
			get { return _nextExpectedAbsolutePosition; }
			set { _nextExpectedAbsolutePosition = value; }
		}

		/// <summary>
		/// The track's current position in relative (0 - 1) coordinates
		/// </summary>
		public Vector2 RelativePosition
		{
			get { return _relativePosition; }
			set { _relativePosition = value; }
		}
		

		/// <summary>
		/// The track's current heading (normalized). Valid if speed is above 0.25 m/s. (Pharus only)
		/// </summary>
		public Vector2 Orientation
		{
			get { return _orientation; }
			set { _orientation = value; }
		}

		/// <summary>
		/// The track's current speed in meters per second (Pharus only)
		/// </summary>
		public float Speed
		{
			get { return _speed; }
			set { _speed = value; }
		}

		/// <summary>
		/// A list of the track's echoes (feet) as Vector2
		/// </summary>
		public List<Vector2> Echoes
		{
			get { return _echoes; }
			set { _echoes = value; }
		}
		
		public List<Vector2> EchoScreenPosition
		{
			get { return _screenPositionEcho; }
			set { _screenPositionEcho = value; }
		}

		public virtual void SetPosition(Vector2 position)
		{
			this.transform.position = position;
		}
		
		void Update()
		{
			// This is just an illustrative example. Adapt it based on where you handle echo data.
			//Debug.Log($"TrackingEntity ID={this.TrackID} has {this.Echoes.Count} Echo(es)");
		}



		/*private void OnDrawGizmos()
		{
			Gizmos.color = Color.red;

			// DEBUG DRAW ECHOES
			foreach (Vector2 echo in Echoes)
			{
				//Gizmos.DrawWireSphere(UnityTracking.TrackingAdapter.GetScreenPositionFromRelativePosition(echo.x, echo.y), 12f);
				
			}
		}*/
	}
}