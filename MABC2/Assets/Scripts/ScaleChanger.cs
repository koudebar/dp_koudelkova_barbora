/*-----------------------------------------
 * \file: ScaleChanger.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: February 2024
 * \brief: Change scale of object in time.Not currently used.
 -----------------------------------------
*/

using UnityEngine;

public class ScaleChanger : MonoBehaviour
{
    public Vector3 minScale = new Vector3(1f, 1f, 1f); // Minimum scale
    public Vector3 maxScale = new Vector3(2f, 2f, 2f); // Maximum scale
    public float scaleSpeed = 1f; // Speed of the scale change

    private Vector3 targetScale; // Current target scale
    private bool scalingUp = true; // Direction of scale change

    void Start()
    {
        targetScale = maxScale; // Start by scaling up
    }

    void Update()
    {
        // Interpolate the scale of the GameObject towards the target scale
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, scaleSpeed * Time.deltaTime);

        // Check if the scale is close to the target scale
        if (Vector3.Distance(transform.localScale, targetScale) < 0.01f)
        {
            // If currently scaling up, switch to scaling down, and vice versa
            if (scalingUp)
            {
                targetScale = minScale;
                scalingUp = false;
            }
            else
            {
                targetScale = maxScale;
                scalingUp = true;
            }
        }
    }
}