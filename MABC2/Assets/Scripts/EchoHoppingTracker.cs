/*-----------------------------------------
 * \file: EchoHoppingTracker.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Tracks the hopping sequence of a child on mat tiles in BAL3, identifying successful completion and triggering animations for errors. Every mat tile in BAL3 has this script attached.
 -----------------------------------------
*/

using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;

public class EchoHoppingTracker : MonoBehaviour
{
    public int matIndex; // (1 to 6)
    public bool finalMat = false;
    public GameObject actionObject; // Public IceTile to be manipulated
    
    private Animation tutorial;
    private bool matWasOccupied = false; // Track if mat was previously occupied
    private int occupatorID = -1;
    private float decideIfValidEchoInterval = 1f; // Jumping is registered correctly iff the person was on tile for one second.
    private float echoDetectionStartTime; // register echo only if entity was standing for some time on the tile. Prevents hopping over echo (Pharus still registers as an entity)
    private float entityDetectionStartTime;
    private bool echoLoggedAlready; // because i measure entity start time in every detection and overwrite, it needs to be logged first.
    private bool entityLoggedAlready;
    private bool loggedAlready = false;
    private bool triggeredOnce; // For animating the icebrick fallout - child did not reached final mat and turned around.
    private DetectIfInShape detector; // Reference to the DetectIfInShape script
    private bool echoRegistered = false; // To check if first echo is registered
    private bool wasPrintedOut = false; // Debug console only message
    private float lastEchoTime = -1; // Time when the last echo was detected
    private bool tutorialPlayed;
    [CanBeNull] private AudioSource completionSound;
    [CanBeNull] private AudioSource completionFailedSound;
    
    void Start()
    {
        GameObject tutorialGameObject = GameObject.Find("Tutorial");
        if (tutorialGameObject != null)
        {
            tutorial = tutorialGameObject.GetComponent<Animation>();
            if (tutorial == null)
            {
                Debug.LogError("No Animation component found on " + tutorialGameObject.name);
            }
        }
        else
        {
            Debug.LogError("GameObject not found in the scene");
        }
        GameManager.Instance.matTimes[matIndex] = 0.0f; // Initialize time for this mat
        GameManager.Instance.matTimeDifference[matIndex] = 0.0f;
        detector = GetComponent<DetectIfInShape>();
        if (finalMat)
        {
            completionFailedSound = GameObject.Find("/Audio/CompletedBadly").GetComponent<AudioSource>();
            completionSound = GameObject.Find("/Audio/Completed").GetComponent<AudioSource>();
        }
    }

    void Update()
    {
        if (GameManager.Instance.reachedFinalMat)
        {
            ResetMeasuring();
        }

        if (EchoDetected())
        {
            if (Time.time - echoDetectionStartTime >= decideIfValidEchoInterval)
            {
                HandleEchoDetection();
            }
        }

        
        //Just for icetile animation
        occupatorID = EntityDetected();
        if (occupatorID != -1 && Time.time - entityDetectionStartTime >= decideIfValidEchoInterval && GameManager.Instance.steppedOnStart)
        {
            matWasOccupied = true; // Mat becomes occupied
        }
        else if (occupatorID == -1 && matWasOccupied && GameManager.Instance.steppedOnStart)
        {
            matWasOccupied = false;
            TriggerActionOnObject(); //play destruction
        }
    }

    // Checks if there are any echoes detected in the area
    private bool EchoDetected()
    {
        bool retval = false;
        if (detector != null && detector.entitiesInsideArea.Count > 0)
        {
            retval = true;
            if (!echoLoggedAlready)
            {
                echoLoggedAlready = true;
                echoDetectionStartTime = Time.time;
            }
        }

        return retval;
    }

    private int EntityDetected()
    {
        int retval = -1;
        if (detector != null && detector.entitiesInsideArea != null && detector.entitiesInsideArea.Count > 0)
        {
            if (!entityLoggedAlready)
            {
                entityLoggedAlready = true;
                entityDetectionStartTime = Time.time;
            }
            foreach (var entity in detector.entitiesInsideArea)
            {
                retval = entity.TrackID;
            }
        }

        return retval;
    }

    private void HandleEchoDetection()
    {
        float currentTime = Time.time;

        if (!echoRegistered)
        {
            // Record the time of the first echo on this mat
            lastEchoTime = currentTime;
            echoRegistered = true;
            GameManager.Instance.matTimes[matIndex] = lastEchoTime;
            LogManager.Instance.LogMessage($"First echo on mat {matIndex} detected in {GameManager.Instance.matTimes[matIndex]}.");
        }
        else
        {
            // Log the time difference between first echoes on sequential mats
            if (echoRegistered && matIndex > 1)
            {
                GameManager.Instance.matTimeDifference[matIndex] = GameManager.Instance.matTimes[matIndex] - GameManager.Instance.matTimes[matIndex-1];
                if (!wasPrintedOut)
                {
                    wasPrintedOut = true;
                    if (GameManager.Instance.matTimes[matIndex - 1] == 0 || !GameManager.Instance.steppedOnStart)
                    {
                        GameManager.Instance.isValidMeasurement = false;
                        LogManager.Instance.LogMessage($"Incorrect trial. Child jumped over more mats.");
                        tutorial.Stop();
                        CountMats();
                        
                    }
                    else
                    {
                        LogManager.Instance.LogMessage($"Time from mat {matIndex-1} to mat {matIndex}: {GameManager.Instance.matTimeDifference[matIndex]}s");
                        GameManager.Instance.numberOfContinuousHops += 1;
                    }
                }
            }

            if (finalMat && !loggedAlready)
            {
                GameManager.Instance.steppedOnStart = false;
                LogManager.Instance.LogMessage("Completed all mats.");
                loggedAlready = true;
                GameManager.Instance.reachedFinalMat = true;
                if (GameManager.Instance.isValidMeasurement)
                {
                    completionSound.Play();
                    CountMats();
                }
                else
                {
                    completionFailedSound.Play();
                }

                
            }

            if (matIndex == 1)
            {
                GameManager.Instance.steppedOnStart = true;
                if (!tutorialPlayed)
                {
                    tutorial.Play("HoppingMat");
                    //Play tutorial commentary
                    if (GameManager.Instance.oralExplanation)
                    {
                        tutorial.GetComponentInChildren<AudioSource>().Play();
                    }

                    tutorialPlayed = true;  // This ensures the animation only plays once
                }
            }
        }
    }

    public void ResetMeasuring()
    {
        lastEchoTime = -1; // Time when the last echo was detected
        echoRegistered = false; // To check if first echo is registered
        wasPrintedOut = false; // Debug console only message
        matWasOccupied = false; // Track if mat was previously occupied
        occupatorID = -1;
        loggedAlready = false;
        echoDetectionStartTime = 0f;
        entityDetectionStartTime = 0f;
        echoLoggedAlready = false;
        entityLoggedAlready = false;
        triggeredOnce = false;
        if (tutorial != null)
        {
            tutorialPlayed = false;
            tutorial.Play("HoppingIdle");
        }
        
        GameManager.Instance.matTimes = new float[7];
        GameManager.Instance.matTimeDifference = new float[7];
        GameManager.Instance.isValidMeasurement = true;
        GameManager.Instance.numberOfContinuousHops = 0;

        //come back to idle animation
        var triggerable = actionObject.transform.GetComponentInChildren<DelayedAnimationStart>();
        triggerable.PlayAnimation();
    }


    // Trigger animation to tile when condition is met (mat is vacated)
    private void TriggerActionOnObject()
    {
        if (actionObject != null && !finalMat &&!triggeredOnce)
        {
            var triggerable = actionObject.transform.GetComponentInChildren<AnimationAndMusicTrigger>();
            if (triggerable != null)
            {
                triggeredOnce = true;
                triggerable.triggerCondition = true;
            }
        }
    }
    
    // log how many succesfull hops.
    private void CountMats()
    {
        if(GameManager.Instance.isValidMeasurement)LogManager.Instance.BAL3 = 5; //All mats completed as expected.
        //count the trials
        if (LogManager.Instance.BAL3 < GameManager.Instance.numberOfContinuousHops)
        {
            LogManager.Instance.LogMessage($"Made {GameManager.Instance.numberOfContinuousHops} consecutive hops.");
            LogManager.Instance.BAL3 = GameManager.Instance.numberOfContinuousHops;
        }
        GameManager.Instance.numberOfContinuousHops = 0;
    }

}
