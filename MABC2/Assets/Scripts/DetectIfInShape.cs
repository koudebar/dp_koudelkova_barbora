/*-----------------------------------------
 * \file: DetectIfInShape.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: February 2024
 * \brief: Detects which tracked entities and echoes are within a specified area, and updates the lists of detected entities and echoes every frame.
 -----------------------------------------
*/

using System;
using System.Collections.Generic;
using DeepSpace.LaserTracking;
using UnityEngine;

public class DetectIfInShape : MonoBehaviour
{
    public TrackingEntityManager trackingManager; // Reference to TrackingEntityManager

    public List<TrackingEntity> entitiesInsideArea; // Save list for other scripts which entities are inside

    public List<Vector2> echoesInsideArea; // same applies for echoes

    // Allow other scripts to check if a specific entity is inside the area - use the id.
    public bool IsSpecificEntityInArea(TrackingEntity entity)
    {
        if (entity == null) return false;
        return IsEntityInArea(entity);
    }

    private void Awake()
    {
        entitiesInsideArea = new List<TrackingEntity>();
        echoesInsideArea = new List<Vector2>();
    }

    // Unity's Update method is called once per frame
    private void Update()
    {
        // Clear the list every frame and repopulate
        entitiesInsideArea.Clear();
        echoesInsideArea.Clear();
        CheckEntitiesInArea();
    }

    private void CheckEntitiesInArea()
    {
        // Ensure we have a valid reference to the TrackingEntityManager
        if (trackingManager == null) return;

        foreach (var entity in trackingManager.TrackingEntityList)
        {
            if (entity == null || entity.gameObject == null)
            {
                Debug.LogWarning($"Encountered a missing entity in the tracking manager.");
                continue; // Skip missing entities
            }
            
            if (IsEntityInArea(entity))
            {
                // Entity is inside the area, print to debug log
                //Debug.Log($"Entity with ID {entity.TrackID} is inside the area.");
                entitiesInsideArea.Add(entity);
            }

            //expected to be utmost two of them
            foreach (var entityEcho in entity.EchoScreenPosition)
            {
                if (IsEchoInArea(entityEcho))
                {
                    echoesInsideArea.Add(entityEcho);
                }
            }

        }
    }

    private bool IsEntityInArea(TrackingEntity entity)
    {
        // Get the RectTransform component of the image that represents the area
        RectTransform areaRectTransform = GetComponent<RectTransform>();

        // Since we are in World Space, we'll use the corners of the RectTransform to create the bounding box
        Vector3[] worldCorners = new Vector3[4];
        areaRectTransform.GetWorldCorners(worldCorners); // Bottom left, top left, top right, bottom right

        // Log the corners of the rectangle
        //Debug.Log(
        // $"World Space Rectangle Corners: BL {worldCorners[0].ToString()}, TL {worldCorners[1].ToString()}, TR {worldCorners[2].ToString()}, BR {worldCorners[3].ToString()}");

        // Create a Rect from the RectTransform's world corners
        Vector2 bottomLeft = worldCorners[0];
        Vector2 topRight = worldCorners[2];
        Rect worldSpaceRect =
            new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);

        // Log the constructed rectangle
        //Debug.Log($"World Space Rectangle: {worldSpaceRect.ToString()}");

        // Get the world position of the entity
        Vector3 entityWorldPosition = entity.transform.position;

        // Log the entity's world position
        //Debug.Log($"Entity {entity.TrackID.ToString()} World Position: {entityWorldPosition.ToString()}");

        // Check if the entity's world position is within the Rect bounds
        // Note: The entity's z-coordinate is not considered in the 2D Rect.
        bool inside = worldSpaceRect.Contains(new Vector2(entityWorldPosition.x, entityWorldPosition.y));

        // Log the result of the containment check
        //Debug.Log($"Is Entity {entity.TrackID.ToString()} inside the area? {inside}");

        return inside;

    }

    private bool IsEchoInArea(Vector2 echoScreenPosition)
    {
        // Get the RectTransform component of the image that represents the area
        RectTransform areaRectTransform = GetComponent<RectTransform>();

        // Since we are in World Space, we'll use the corners of the RectTransform to create the bounding box
        Vector3[] worldCorners = new Vector3[4];
        areaRectTransform.GetWorldCorners(worldCorners); // Bottom left, top left, top right, bottom right

        // Create a Rect from the RectTransform's world corners
        Vector2 bottomLeft = worldCorners[0];
        Vector2 topRight = worldCorners[2];
        Rect worldSpaceRect =
            new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);

        // Determine the appropriate camera for the conversion
        Camera echoCamera = Camera.main;
        
        //Camera echoCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

        // Check if the echoCamera is null to avoid errors
        if (echoCamera == null)
        {
            Debug.LogError("No echo-detecting camera is not found");
            return false;
        }
        
        //Debug.Log($"Echo screen Pos: {echoScreenPosition.x}, {echoScreenPosition.y}");
        // Convert screen position echo to world position
        Vector3 echoWorldPosition = echoCamera.ScreenToWorldPoint(new Vector3(echoScreenPosition.x, echoScreenPosition.y, 0.0f));

        // Check if the echo's world position is within the Rect bounds
        bool inside = worldSpaceRect.Contains(new Vector2(echoWorldPosition.x, echoWorldPosition.y));
        //Debug.Log($"Echo World Position: {echoWorldPosition.ToString()}");

        return inside;
    }

}