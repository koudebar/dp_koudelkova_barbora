/*-----------------------------------------
 * \file: GameManager.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Manages global gameplay state.
 *         Implements a singleton pattern and provides methods to reset variables and handle scene changes.
 -----------------------------------------
*/

using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using DeepSpace.LaserTracking;

public class GameManager : MonoBehaviour
{
    // Singleton instance
    public static GameManager Instance;

    // Global variables
    public string type = "undefined";
    public int isOperator = -1; //-1 not selected, 0 Host, 1 Operator
    public bool isOperatorOverride = false; //if has tablet attached
    public bool wasRaisedRecording = false;
    public bool SceneNameChanged = false; // Flag to indicate a request for scene change, thread-safe
    public string SceneName = "";
    public Vector3 xyCoordsOfDetection; // The first entity position that landed on object
    public Vector3 xyCoordsOfEchoDetection; // The first echo position that landed on object
    public bool oralExplanation = false;

    /*public int AC1_counter = 0;
    public */

    //*
    // AC varibales
    //*
    
    public bool isStandingOnPlatform = false;
    public bool objectWasThrown = false;
    public TrackingEntity standingEntity; // Remember who stands on platform
    public Vector3 xyCoordsOfBallThrow;
    public bool wasDetectedSecondObject = false;
    public bool wasDetectedBall = false;
    public bool therapistInCircle;
    public bool AC1_correctCatch;
    public bool AC1_incorrectCatch;

    //*
    // BAL variables
    //*

    // Walking On Line
    public bool isStandingOnStart = false;
    public int walkingCorrectly = -1; //-1 if not set, 0 not walking on line, 1 correctly on line 
    public bool finishedWalking = false; //player finished walking
    public bool exportData; // Export data
    public event Action<int> OnEchoesChanged; // Event to subscribe to
    private int howManyEchoes; // How many echoes appeared on the line

    // Hopping On Mats
    public float[] matTimes; // Store times of echoes per mat in runtime
    public float[] matTimeDifference; // Store calculated time differences
    public bool reachedFinalMat;
    public bool steppedOnStart;
    public bool isValidMeasurement = true;
    public int numberOfContinuousHops; // How many hops child achieved before it screw up.
    //*
    // Private runtime variables
    //*
    private bool hoppingLogged;
    private float starttime;
    
    
    //----Subscribe event on changing echoes
    public int HowManyEchoes
    {
        get => howManyEchoes;
        set
        {
            if (howManyEchoes != value)
            {
                howManyEchoes = value;
                OnEchoesChanged?.Invoke(howManyEchoes); // Invoke event when value changes
            }
        }
    }
    
    
    private void Awake()
    {
        // Create or set the instance
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); // Keeps the GameManager object alive between scenes
            //wasRaisedRecording = false; // Just in case someone forgot to stop recording
        }
        else
        {
            Destroy(gameObject); // Destroy duplicate instances
        }
    }

    private void Start()
    {
        // Initialize arrays to hold times for 6 mats + 1 because i am lazy pig to index from zero :P
        matTimes = new float[7];
        matTimeDifference = new float[7];
    }
    
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reset variables when a new scene is loaded
        ResetGameVariables();
    }

    public void ResetGameVariables()
    {
        // Reset all exercise-related the variables to their initial values
        //isOperatorOverride = false;
        //wasRaisedRecording = false;

        isStandingOnPlatform = false;
        objectWasThrown = false;
        wasDetectedSecondObject = false;
        isStandingOnStart = false;
        walkingCorrectly = -1;
        reachedFinalMat = false;
        finishedWalking = false;
        exportData = false;
        wasDetectedBall = false;
        steppedOnStart = false;
        isValidMeasurement = true;
        therapistInCircle = false;
        AC1_correctCatch = false;
        AC1_incorrectCatch = false;
        numberOfContinuousHops = 0;
        
        xyCoordsOfDetection = new Vector3();
        xyCoordsOfEchoDetection = new Vector3();
        xyCoordsOfBallThrow = new Vector3();
    }

    public void Update()
    {
        //set the finalMatreach to false automatically after 2 seconds, because of the echo hopping and rerunning test again.
        if (reachedFinalMat && !hoppingLogged)
        {
            starttime = Time.time;
            hoppingLogged = true;
        }

        if (Time.time - starttime >= 2 && hoppingLogged)
        {
            reachedFinalMat = false;
            hoppingLogged = false;
        }
    }

    private void OnDestroy()
    {
        // Unsubscribe from the SceneManager.sceneLoaded event when the game object is destroyed
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}