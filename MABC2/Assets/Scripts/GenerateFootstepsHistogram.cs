/*-----------------------------------------
 * \file: GenerateFootstepsHistogram.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Generates a histogram of detected footsteps and heel raises based on echo data within a defined area.
 *         The algorithm identifies footsteps by counting echoes in specific bins,
 *         Suppresses neighboring bins to avoid double-counting, and logs the results to CSV files via LogManager.
 -----------------------------------------
*/

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GenerateFootstepsHistogram : MonoBehaviour
{
    private DetectIfInShape detector;
    public int numberOfBins = 128;
    public int numberOfEchoesAsStepsThreshold = 72;
    public int numberOfEchoesAsHeelRaisedThreshold = 100;
    public int suppressionRange = 7;
    private float[] echoHistogram;
    private int[] echoRegisteredAsFootstep;
    private int[] echoRegisteredAsHeelRaise;
    private bool[] footstepSuppression;
    private float minX;
    private float maxX;
    private bool logging; // delay between exporting to csv and logging, since it resets before it actually writes to file
    private int measureNumber = 0; //For export of Balance
    private float stepDetectionFinish = 0.0f; //player can stand for a while on finish, and since we do not resolve that and this script only sets it to false, lets log the histogram after a while


    private void Start()
    {
        detector = GetComponent<DetectIfInShape>();
        echoHistogram = new float[numberOfBins];
        echoRegisteredAsFootstep = new int[numberOfBins];
        echoRegisteredAsHeelRaise = new int[numberOfBins];
        footstepSuppression = new bool[numberOfBins];

        // Attempt to use RectTransform to set minX and maxX
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (rectTransform != null)
        {
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);
            minX = corners[0].x; // Bottom-left corner in world space
            maxX = corners[3].x; // Bottom-right corner in world space
        }
        else
        {
            Debug.LogError(
                "No RectTransform found on GameObject. Please ensure this script is attached to an object with a RectTransform.");
            minX = -10f; // Default fallback value
            maxX = 10f; // Default fallback value
        }
    }


    void Update()
    {   //Player ended with walking, now its the time to export it and reset again.
        if (GameManager.Instance.exportData)
        {
            LogManager.Instance.LogHistogram(measureNumber, echoHistogram, echoRegisteredAsFootstep, echoRegisteredAsHeelRaise);
            stepDetectionFinish = Time.time;
            GameManager.Instance.exportData = false;
            logging = true;
        }

        if (Time.time - stepDetectionFinish >= 0.9 && logging)
        {
            stepDetectionFinish = 0;
            resetHistogramValues();
        }

        if (detector != null && detector.echoesInsideArea.Count > 0)
        {
            UpdateHistogram(detector.echoesInsideArea);
        }
    }


    private void UpdateHistogram(List<Vector2> echoes)
    {
        foreach (Vector2 echo in echoes)
        {
            int binIndex = Mathf.FloorToInt((echo.x - minX) / (maxX - minX) * numberOfBins);
            if (binIndex >= 0 && binIndex < numberOfBins)
            {
                echoHistogram[binIndex]++;
                if (echoHistogram[binIndex] > numberOfEchoesAsStepsThreshold && !footstepSuppression[binIndex])
                {
                    echoRegisteredAsFootstep[binIndex] = 1;
                    SuppressNeighboringBins(binIndex);
                    GameManager.Instance.HowManyEchoes++;
                    LogManager.Instance.LogMessage($"Step: {GameManager.Instance.HowManyEchoes} registered from index of bin: {binIndex}");
                }
                if (echoHistogram[binIndex] > numberOfEchoesAsHeelRaisedThreshold && echoRegisteredAsHeelRaise[binIndex] == 0)
                {
                    echoRegisteredAsHeelRaise[binIndex] = 1;
                    LogManager.Instance.LogMessage($"Step: {binIndex} registered as heel raise");
                }
            }
        }

        //DebugHistogram();
    }

    private void SuppressNeighboringBins(int centralBin)
    {
        int startBin = Mathf.Max(0, centralBin - suppressionRange);
        int endBin = Mathf.Min(numberOfBins - 1, centralBin + suppressionRange);

        for (int i = startBin; i <= endBin; i++)
        {
            footstepSuppression[i] = true;
        }
    }

    private void DebugHistogram()
    {
        string histogramString = "Histogram: ";
        for (int i = 0; i < echoHistogram.Length; i++)
        {
            histogramString += echoHistogram[i] + " ";
        }

        Debug.Log(histogramString);
    }

    public void resetHistogramValues()
    {
        echoHistogram = new float[numberOfBins];
        echoRegisteredAsFootstep = new int[numberOfBins];
        echoRegisteredAsHeelRaise = new int[numberOfBins];
        footstepSuppression = new bool[numberOfBins];
        GameManager.Instance.HowManyEchoes = 0;
        measureNumber++;
        stepDetectionFinish = 0.0f;
        logging = false;
    }
}