/*-----------------------------------------
 * \file: NetworkBroadcaster.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Manages UDP network broadcasting and listening for coordinating scene changes between host and tablet and via loopback the recording status with pzthon mediapipe application. 
 -----------------------------------------
*/

using System;
using System.Collections;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkBroadcaster : MonoBehaviour
{
    private UdpClient broadcaster;
    private UdpClient listener;
    private IPEndPoint broadcastEndPoint;
    private IPEndPoint listenEndPoint;
    private Thread listenThread;
    private bool isBroadcasting = false;
    private bool isListening = false;
    private Coroutine broadcastCoroutine;
    
    private volatile bool requestStopBroadcasting = false;

    void Start()
    {
        int port = 8888; // The port for both broadcasting and listening
        broadcaster = new UdpClient();
        listener = new UdpClient(port);
        listenEndPoint = new IPEndPoint(IPAddress.Any, port);
        broadcastEndPoint = new IPEndPoint(IPAddress.Broadcast, port);
        broadcaster.EnableBroadcast = true;
        listener.EnableBroadcast = true;

    }
    
    /*Thread-safe way to change variables from main thread. otherwise it fails load scene tremendously*/
    void Update()
    {
        // Check if the scene needs to be changed, and if so, do it on the main thread
        if (GameManager.Instance.SceneNameChanged)
        {
            ReloadScene(GameManager.Instance.SceneName);
            GameManager.Instance.ResetGameVariables();
            GameManager.Instance.SceneNameChanged = false; // Reset the flag
        }
    }
    
    private void ReloadScene(string sceneName)
    {
        // Check if the requested scene is the current scene
        if (SceneManager.GetActiveScene().name == sceneName)
        {
            // Reload the same scene
            SceneManager.LoadScene(sceneName);
        }
        else
        {
            // Load a different scene
            SceneManager.LoadScene(sceneName);
        }
    }

    public void OnClick()
    {
        //yield new WaitForSeconds(1.0f);
        // Decide whether to broadcast or listen based on GameManager.Instance.isOperator
        if (GameManager.Instance.isOperator == 1)
        {
            Debug.Log("waiting to broadcasting TABLET_READY");
            StartListening();
            StartBroadcasting("TABLET_READY");
        }
        else if (GameManager.Instance.isOperator == 0)
        {
            StartListening();
            
            StartBroadcasting("HOST_SESSION_ACTIVE");
        }
    }

    void StartListening()
    {
        if (!isListening)
        {
            isListening = true;
            listenThread = new Thread(new ThreadStart(ListenForMessages));
            listenThread.IsBackground = true;
            listenThread.Start();
        }
    }

    void ListenForMessages()
    {
        // Handshake for starting the connection
        while (isListening)
        {
            try
            {
                byte[] receivedBytes = listener.Receive(ref listenEndPoint);
                string receivedMessage = Encoding.UTF8.GetString(receivedBytes);
                Debug.Log("Received");
                Debug.Log(receivedMessage);
                if (receivedMessage == "HOST_SESSION_ACTIVE" && GameManager.Instance.isOperator == 1)
                {
                    listener.Close();
                    isListening = false;
                    Debug.Log("HOST_SESSION_ACTIVE received, switching to broadcasting TABLET_READY");
                    
                }
                else if(receivedMessage == "PAIRED" && GameManager.Instance.isOperator == 1)
                {
                    requestStopBroadcasting = true;
                }
                else if (receivedMessage == "TABLET_READY" && GameManager.Instance.isOperator == 0)
                {
                    GameManager.Instance.isOperatorOverride = true;
                    //StopBroadcasting();
                    requestStopBroadcasting = true;
                    //send ACK once
                   /* byte[] sendBytes = Encoding.UTF8.GetBytes("PAIRED");
                    broadcaster.Send(sendBytes, sendBytes.Length, broadcastEndPoint);
                    Debug.Log($"Sending: PAIRED");*/
                }
                //NOTICE! since this does not work as expected, that is why it is commented out, 
                else if (receivedMessage == "RECORDING" /*&& GameManager.Instance.isOperator == 0*/)
                {
                    if (GameManager.Instance.wasRaisedRecording) GameManager.Instance.wasRaisedRecording = false;
                    else
                    {
                        GameManager.Instance.wasRaisedRecording = true;
                    }
                    Debug.Log($"Obtained recording.");
                }
                else if (receivedMessage == "EXPLAIN" /*&& GameManager.Instance.isOperator == 0*/)
                {
                    if (GameManager.Instance.oralExplanation) GameManager.Instance.oralExplanation = false;
                    else
                    {
                        GameManager.Instance.oralExplanation = true;
                    }
                    Debug.Log($"Obtained request for explanation."); 
                }
                else if (GameManager.Instance.isOperator == 0)
                {
                    if (receivedMessage == "" || receivedMessage == "TABLET_READY" ||
                        receivedMessage == "HOST_SESSION_ACTIVE" || receivedMessage == "PAIRED") continue;
                    GameManager.Instance.SceneName = receivedMessage;
                    GameManager.Instance.SceneNameChanged = true;
                    Debug.Log($"Obtained change scene demand: {receivedMessage}");
                }

            }
            catch (Exception e)
            {
                Debug.LogError($"Listening error: {e.Message}");
            }
        }
    }

    public void StartBroadcasting(string message)
    {
        if (!isBroadcasting )
        {
            isBroadcasting = true;
            broadcastCoroutine =  StartCoroutine(BroadcastMessage(message)); // Changed to use Coroutine for broadcasting
        }
    }

    IEnumerator BroadcastMessage(string message)
    {
        while (isBroadcasting && !requestStopBroadcasting)
        {
            try
            {
                byte[] sendBytes = Encoding.UTF8.GetBytes(message);
                broadcaster.Send(sendBytes, sendBytes.Length, broadcastEndPoint);
                //Debug.Log($"Broadcasting: {message}");
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to send broadcast: {e.Message}");
            }
            // Reset the request flag and any cleanup after broadcasting stops
            //requestStopBroadcasting = false;
            //isBroadcasting = false;
            yield return new WaitForSeconds(1f); // Wait for 1 second before the next broadcast
        }
    }

    void OnDisable()
    {
        isBroadcasting = false;
        if (listener != null)
        {
            listener.Close();
        }

        if (broadcaster != null)
        {
            broadcaster.Close();
        }

        isListening = false;
        if (listenThread != null && listenThread.IsAlive)
        {
            listenThread.Abort();
        }
    }

    public void SceneClick()
    {
        try
        {
            byte[] sendBytes = Encoding.UTF8.GetBytes(GameManager.Instance.SceneName);
            broadcaster.Send(sendBytes, sendBytes.Length, broadcastEndPoint);
            Debug.Log($"Broadcasting: {GameManager.Instance.SceneName}");
        }
        catch (Exception e)
        {
            Debug.Log("Failed.");
        }
    }

    public void RecordingClick()
    {
        try
        {
            byte[] sendBytes = Encoding.UTF8.GetBytes("RECORDING");
            broadcaster.Send(sendBytes, sendBytes.Length, broadcastEndPoint);
            Debug.Log($"Broadcasting: RECORDING");
        }
        catch (Exception e)
        {
            Debug.Log("Failed.");
        }
    }
    
    public void OralExplanationClick()
    {
        try
        {
            byte[] sendBytes = Encoding.UTF8.GetBytes("EXPLAIN");
            broadcaster.Send(sendBytes, sendBytes.Length, broadcastEndPoint);
            Debug.Log($"Broadcasting: EXPLAIN");
        }
        catch (Exception e)
        {
            Debug.Log("Failed.");
        }
    }
}