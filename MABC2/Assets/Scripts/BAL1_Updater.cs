/*-----------------------------------------
 * \file: BAL1_Updater.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Manages the visual progression of the forest and water in well in BAL1 based on player standing trials, and listens for UDP messages to trigger trial updates and tutorials.
 -----------------------------------------
*/

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class BAL1_Updater : MonoBehaviour
{
    public GameObject growingForest;
    public GameObject wellPrefab;
    public GameObject stepZone;
    public GameObject moveWater;
    public GameObject tutorial;
    public AudioSource victoryMusic;
    public bool debugStanding;
    public bool debugSendStartMessage;
    public bool ignoreStartPosition;

    private UdpClient udpClient;
    private IPEndPoint remoteEndPoint;
    private const int listenPort = 1234;
    private float forestStartingPosition = 5414f; //hardcoded values for position because it waa the easiest and quickest way today
    private float forestEndPosition = 3032f;
    private float waterStartingPosition = -0.252f; // LOCAL hardcoded values for position because it waa the easiest and quickest way today
    private float waterEndPosition = 0.277f;
    private bool isListening = false;
    private float startTime = 0;
    private float lastMessageTime;  // Timer to track the last time a message was received
    private bool messageReceivedContinuously = false;
    private bool targetReached = false;
    private int leftFootTrials = 0;
    private int rightFootTrials = 0;
    private string lastFoot = "";
    private float trialStartTime = 0;
    private bool validMeasurement = false;
    private bool setActive = false;
    private bool TutorialPlayed;
    private bool hasLoopedOnce = false; // Let the child play whole animaton first
    private float animationTime = 0;

    void Start()
    {
        StartListening();
        lastMessageTime = Time.time;  // Initialize the last message time
        ResetPosition();
    }

    void Update()
    {
        if (debugStanding)
        {
            messageReceivedContinuously = true;
            validMeasurement = true;
            tutorial.transform.Find("TutorialLeft").gameObject.SetActive(false); //set tutorial to left anim
        }

        if (ignoreStartPosition)
        {
            stepZone.SetActive(false);
            hasLoopedOnce = true;
        }

        if (GameManager.Instance.isStandingOnPlatform)
        {
            if (!setActive)
            {
                animationTime = Time.time;
                setActive = true;
                tutorial.transform.Find("TutorialLeft").gameObject.SetActive(true); //set tutorial to left anim
                if(GameManager.Instance.oralExplanation)tutorial.transform.Find("TutorialLeft").gameObject.transform.Find("Commentary").GetComponent<AudioSource>().Play();
                stepZone.SetActive(false);

            }

            if (Time.time - animationTime >= 7)
            {
                hasLoopedOnce = true;
            }

        }

        if (isListening && hasLoopedOnce) // Start listening only if whole animation was played. Trust me I know why.
        {
            string receivedData = ReceiveData();
            if (!string.IsNullOrEmpty(receivedData))
            {
                lastMessageTime = Time.time;  // Reset the timer on ANY!!!! received message
            }

            if (receivedData.StartsWith("Player is standing on one leg"))
            {
                string foot = receivedData.Split(',')[1].Trim().Split(' ')[1]; // Assumes format "Player is standing on one leg, Foot: Right Foot"
                tutorial.transform.Find("TutorialRight").gameObject.SetActive(false);
                tutorial.transform.Find("TutorialLeft").gameObject.SetActive(false);
                if (!messageReceivedContinuously)
                {
                    messageReceivedContinuously = true;
                    startTime = Time.time;
                }

                if (lastFoot != foot)
                {
                    lastFoot = foot;

                    if (foot == "Left" /*&& leftFootTrials < 2*/)
                    {
                        validMeasurement = true;
                        leftFootTrials++;
                        LogManager.Instance.LogMessage($"Child started standing on the Left Foot. Trial {leftFootTrials}.");
                    }
                    else if (foot == "Right" /*&& rightFootTrials < 2*/)
                    {
                        validMeasurement = true;
                        rightFootTrials++;
                        LogManager.Instance.LogMessage($"Child started standing on the Right Foot. Trial {rightFootTrials}.");
                    }
                }

            }
            
            else if(receivedData.StartsWith("Start") || debugSendStartMessage)
            {
                float timeElapsed = Time.time - startTime;
                //if the player dont know what to do, wait two seconds and then play tutorial again.
                if (timeElapsed >= 1 && !TutorialPlayed)
                {
                    if (lastFoot == "Left")
                    {
                        if (LogManager.Instance.BAL1_left < timeElapsed) LogManager.Instance.BAL1_left = timeElapsed;
                        tutorial.transform.Find("TutorialRight").gameObject.SetActive(true);
                    }
                    else
                    {
                        if (LogManager.Instance.BAL1_right < timeElapsed) LogManager.Instance.BAL1_right = timeElapsed;
                        tutorial.transform.Find("TutorialLeft").gameObject.SetActive(true);
                    }
                    TutorialPlayed = true;
                    if(GameManager.Instance.oralExplanation)  tutorial.transform.Find("TutorialLeft").gameObject.transform.Find("SwapFootAudio").GetComponent<AudioSource>().Play();
                    if(GameManager.Instance.oralExplanation)  tutorial.transform.Find("TutorialRight").gameObject.transform.Find("SwapFootAudio").GetComponent<AudioSource>().Play();
                }

                //Child stopped with standing. Log it.
                if (messageReceivedContinuously && validMeasurement && !targetReached)
                {
                    LogManager.Instance.LogMessage($"Trial duration:{Time.time - startTime} seconds.");

                    //Change the feet
                }
                messageReceivedContinuously = false;
                ResetPosition();
            }
            if (messageReceivedContinuously && validMeasurement)
            {
                float timeElapsed = Time.time - startTime;
                TutorialPlayed = false;
                // Move the forest while child is standing
                if (timeElapsed < 30)
                {
                    MoveForestPosition(timeElapsed / 30);
                    MoveWaterPosition(timeElapsed / 30);
                }
                // Task accomplished
                else if (!targetReached && timeElapsed >= 30)
                {
                    targetReached = true;
                    victoryMusic.Play(); // Play music on successful completion
                    LogManager.Instance.LogMessage($"Trial duration:{timeElapsed} seconds.");
                    LogManager.Instance.LogMessage($"Success.");
                    // Log duration
                    if (lastFoot == "Left")
                    {
                        if (LogManager.Instance.BAL1_left < timeElapsed) LogManager.Instance.BAL1_left = timeElapsed;
                    }
                    else
                    {
                        if (LogManager.Instance.BAL1_right < timeElapsed) LogManager.Instance.BAL1_right = timeElapsed;
                    }
                    //messageReceivedContinuously = false;
                }
            }

            // Reset if no messages are received for 1 second
            if (Time.time - lastMessageTime > 1 && !debugStanding)
            {
                if(messageReceivedContinuously && validMeasurement)LogManager.Instance.LogMessage($"Trial duration:{Time.time - startTime} seconds.");
                messageReceivedContinuously = false;
                ResetPosition();
            }
        }
    }

    void OnDisable()
    {
        StopListening();
    }

    private void StartListening()
    {
        remoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), listenPort);
        udpClient = new UdpClient(listenPort);
        isListening = true;
    }

    private string ReceiveData()
    {
        if (udpClient.Available > 0)
        {
            byte[] receivedBytes = udpClient.Receive(ref remoteEndPoint);
            string receivedText = Encoding.ASCII.GetString(receivedBytes);
            Debug.Log("Received: " + receivedText);
            return receivedText;
        }
        return string.Empty;
    }

    private void StopListening()
    {
        if (udpClient != null)
        {
            udpClient.Close();
            isListening = false;
        }
    }
    
    private void MoveForestPosition(float progress)
    {
        float newPositionZ = Mathf.Lerp(forestStartingPosition, forestEndPosition, progress);
        growingForest.transform.position = new Vector3(growingForest.transform.position.x, growingForest.transform.position.y, newPositionZ);
    }
    
    private void MoveWaterPosition(float progress)
    {
        // Interpolate the water's local position based on the progress
            float newPositionZ = Mathf.Lerp(waterStartingPosition, waterEndPosition, progress);
            moveWater.transform.localPosition = new Vector3(moveWater.transform.localPosition.x, moveWater.transform.localPosition.y, newPositionZ);
        
    }

    private void ResetPosition()
    {
        growingForest.transform.position = new Vector3(growingForest.transform.position.x, growingForest.transform.position.y, forestStartingPosition);
        moveWater.transform.localPosition = new Vector3(moveWater.transform.localPosition.x, moveWater.transform.localPosition.y, waterStartingPosition);
        targetReached = false;
        lastFoot = "";
        //TODO strict 2 measurement policy
        //validMeasurement = false;
    }
}