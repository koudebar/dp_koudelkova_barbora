/*-----------------------------------------
 * \file: AC1_updater.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Handles the AC1 task sequences, including counting trials and managing resets when a child steps out early or successfully throws the ball.
 -----------------------------------------
*/


using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
/*
 *This script launches the sequences tied to AC1 tasks
 * 
 */

public class AC1_updater : MonoBehaviour
{
    public GameObject accomplished; //launch accomplished sequence
    public GameObject stopMeasurement;
    public GameObject startPlace;
    public GameObject stepZone;
    public GameObject therapistSigns;
    public GameObject therapistCircle;
    public int num_trials = 10; //number of trials a child has before scene change
    public bool debug = false;

    private bool detectedStanding = false; // set to true if person stepped inside measurement area
    private bool stopMeasure = false; // set to true if person stepped inside measurement area
    private int count_trials = 0;
    private int half_succesful_trials = 0; //number of trials where child caught the ball
    private UdpClient udpClient;
    private IPEndPoint remoteEndPoint;
    private const int listenPort = 1234;
    private bool isListening;

    private DetectIfInShape detector;
    
    private void Start()
    {
        // Find the DetectIfInShape component on the GameObject this script is attached to
        detector = startPlace.GetComponent<DetectIfInShape>();
    }

    
    public void Update()
    {
        // we are ready to shoot, right?
        if (GameManager.Instance.isStandingOnPlatform && !detectedStanding)
        {
            if (count_trials == 0 && GameManager.Instance.oralExplanation)startPlace.GetComponentInChildren<AudioSource>().Play();
            stepZone.SetActive(false);
            detectedStanding = true;
            accomplished.SetActive(false);
            stopMeasurement.SetActive(false);
        }

        if (GameManager.Instance.therapistInCircle){
        
            //Debug.Log("Standing in circle");
            therapistSigns.SetActive(true);
        }

        // Therapist assessed a good catch
        if (GameManager.Instance.AC1_correctCatch)
        {
            therapistSigns.SetActive(false);
            GameManager.Instance.AC1_correctCatch = false;
            GameManager.Instance.therapistInCircle = false;
            LogManager.Instance.LogMessage("Child caught ball correctly.");
            LogManager.Instance.AC1[count_trials] = 1;
            therapistSigns.SetActive(false);
            CountTrials();
        }
        
        //Bad catch, therapist stepped on red cross.
        if (GameManager.Instance.AC1_incorrectCatch)
        {
            therapistSigns.SetActive(false);
            GameManager.Instance.AC1_incorrectCatch = false;
            GameManager.Instance.therapistInCircle = false;
            LogManager.Instance.LogMessage("Child caught ball incorrectly.");
            LogManager.Instance.AC1[count_trials] = 0;
            therapistSigns.SetActive(false);
            CountTrials();
        }

        if (detectedStanding)
        {
            // child stepped out before completion
            if (!detector.IsSpecificEntityInArea(GameManager.Instance.standingEntity) && !GameManager.Instance.objectWasThrown &&!stopMeasure &&!debug)
             {
                 stopMeasurement.SetActive(true);
                 if(GameManager.Instance.oralExplanation)stopMeasurement.transform.Find("Commentary").GetComponent<AudioSource>().Play();
                 LogManager.Instance.LogMessage("Stopped. Child stepped from mat.");
                 LogManager.Instance.AC1[count_trials] = 0; //It did not go as expected :(
                 CountTrials();
             }
        }
    }

    public void CountTrials()
    {
        count_trials++;
        Debug.Log($"Num of trial: {count_trials}");
        if (num_trials == count_trials)
        {
            LogManager.Instance.LogMessage($"Ten trials done.");
            stepZone.SetActive(false);
            therapistSigns.SetActive(false);
            therapistCircle.SetActive(false);
            //accomplished.SetActive(true);

        }
        else
        {

            ResetToDefault();
        }
    }
    
    private void ResetToDefault()
    {
        GameManager.Instance.objectWasThrown = false;
        GameManager.Instance.isStandingOnPlatform = false;
        GameManager.Instance.wasDetectedSecondObject = false;
        detectedStanding = false;
        stopMeasure = false;
        //stopMeasurement.SetActive(false);
        //accomplished.SetActive(false);
    }
}