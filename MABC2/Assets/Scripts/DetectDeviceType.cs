/*-----------------------------------------
 * \file: DetectDeviceType.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Detect which device is the app on, to prevent tablets hosting sessions.
 -----------------------------------------
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectDeviceType : MonoBehaviour
{
    void Start()
    {
        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            GameManager.Instance.type = "Handheld";
            Debug.Log("Running on a handheld device.");
        }
        else if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            Debug.Log("Running on a desktop device.");
        }

        // Specific platform check
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Debug.Log("App is running on a tablet or smartphone.");
        }
        else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.LinuxPlayer)
        {
            GameManager.Instance.type = "PC";
            Debug.Log("App is running on a PC.");
        }
    }
}
