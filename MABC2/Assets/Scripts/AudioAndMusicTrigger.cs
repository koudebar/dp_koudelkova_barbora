/*-----------------------------------------
 * \file: AnimationAndMusicTrigger.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Triggers an animation and music clip when a specific game condition is met, pariculary managing many sounds of ice tiles in BAL3.
 -----------------------------------------
*/

using UnityEngine;

public class AnimationAndMusicTrigger : MonoBehaviour
{
    public string animationName = "Idle";
    public AudioClip musicClip;

    private Animation animation;
    private AudioSource audioSource;

    public bool triggerCondition = false; 

    void Start()
    {
        animation = GetComponent<Animation>();
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = musicClip;
        audioSource.playOnAwake = false;
    }

    void Update()
    {
        if (triggerCondition) // Check if the condition to play music and animation is met
        {
            triggerCondition = false; // Prevent the condition from triggering repeatedly
            PlayAnimationAndMusic();
        }
    }

    void PlayAnimationAndMusic()
    {
        if (animation != null && audioSource != null)
        {
            Invoke(nameof(StartAnimationAndMusic), 0); // Delay the start
        }
    }

    void StartAnimationAndMusic()
    {
        if (animation != null)
        {
            animation.Play(animationName); // Start the animation
        }
        if (audioSource != null && musicClip != null)
        {
            audioSource.Play(); // Start playing the music
        }
    }
}
