/*-----------------------------------------
 * \file: BAL2_updater.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Controls the visibility of the tutorial, line, and step counter in BAL2, and triggers data export upon completion of the walking trial.
 -----------------------------------------
*/



using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BAL2_updater : MonoBehaviour
{
    public GameObject line;
    public GameObject tutorial;
    public GameObject number_steps;
    public GameObject finishTile;

    private DetectIfInShape detector;
    private AudioSource completed;

    private float tutorialTime;
    private bool tutorialWasPlayed;
    private bool notMadeMistake;
    private bool explainedWasPlayed;
    private int numberOfStepsInSequence; //Remember how many steps child did before a mistake
    private int numberOfStepsInSequencePrev; //Remember how many steps child did before a mistake
    public void Start()
    {
        detector = finishTile.GetComponent<DetectIfInShape>();
        completed = GetComponent<AudioSource>();
    }

    public void Update()
    {
        //Child stepped on start, visualise the line after playing the tutorial
        if (GameManager.Instance.isStandingOnStart)
        {
            GameManager.Instance.finishedWalking = false; // last minute fix becaouse player sometimes triggered the finished area
            if (!tutorial.activeSelf && !tutorialWasPlayed)
            {
                tutorialTime = Time.time;
                tutorial.SetActive(true);
                tutorialWasPlayed = true;
                notMadeMistake = true;
                numberOfStepsInSequence = 0;
                numberOfStepsInSequencePrev = 0;

            }else if (GameManager.Instance.oralExplanation && Time.time - tutorialTime >=2.5 && !explainedWasPlayed) // Start the oral explanation with delay, since it is shorter than the animated tutorial
            {
                tutorial.transform.Find("Commentary").GetComponent<AudioSource>().Play();
                explainedWasPlayed = true;

            }
            else if (Time.time - tutorialTime >= 11) //tutorial play ended (hardcoded since we do not use Animator but Animation component)
            {
                line.SetActive(true);
                number_steps.SetActive(true);
                GameManager.Instance.isStandingOnStart = false;
                tutorial.SetActive(false);
            }

        }

        CheckIfWalkingCorrectly();
        
        //Child stepped to finishline and stepped out, proceed with another trial and generate data in different scripts.
        if (GameManager.Instance.finishedWalking && !detector.IsSpecificEntityInArea(GameManager.Instance.standingEntity))
        {
            GameManager.Instance.exportData = true;
            completed.Play();
            line.SetActive(false);
            number_steps.SetActive(false);
            GameManager.Instance.finishedWalking = false;
            tutorialWasPlayed = false;
            explainedWasPlayed = false;
            notMadeMistake = true;
        }


    }
    
    // Method for subsequent evaluation, if child is walking correctly, according to MABC directive

    //For the low quality and echoes obtained from single lidar setup, only the counted steps are passed,
    //but the code logic works - the echoes are ranging up to 1m in vertical axis, so it always detects false.
    void CheckIfWalkingCorrectly()
    {
        if (GameManager.Instance.finishedWalking)
        {
            if (notMadeMistake)
            {
                LogManager.Instance.BAL2steps = 15;
            }
            else if(LogManager.Instance.BAL2steps < numberOfStepsInSequence)
            {
            // Uncomment this when better sensor setup will be avaiable
                //LogManager.Instance.BAL2steps = numberOfStepsInSequence;
                 LogManager.Instance.BAL2steps = GameManager.Instance.HowManyEchoes;
            }


        }else if (GameManager.Instance.walkingCorrectly == 1)
        {
            numberOfStepsInSequence = GameManager.Instance.HowManyEchoes - numberOfStepsInSequencePrev;
            if (LogManager.Instance.BAL2steps < numberOfStepsInSequence)
            {
                LogManager.Instance.BAL2steps = numberOfStepsInSequence;
            }

        }else if (GameManager.Instance.walkingCorrectly == 0)
        {
            notMadeMistake = false;
            numberOfStepsInSequencePrev += numberOfStepsInSequence;
            numberOfStepsInSequence = 0;
            LogManager.Instance.BAL2steps = 0;
        }

    }
}
