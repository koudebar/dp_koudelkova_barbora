/*-----------------------------------------
 * \file: DisplayStepsUI.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Displays the number of echoes detected as steps in BAL2 by updating a UI text element when the step count changes.
 -----------------------------------------
*/


using TMPro;
using UnityEngine;
using UnityEngine.UI;

/*DISPLAY HOW MANY ECHOES HAS BEEN ON LINE*/
public class DisplayStepsUI : MonoBehaviour
{
    public TextMeshProUGUI stepsText; // Assign in the inspector

    private void OnEnable()
    {
        // Subscribe to the event
        GameManager.Instance.OnEchoesChanged += UpdateStepsText;
    }

    private void OnDisable()
    {
        // Unsubscribe to prevent memory leaks
        GameManager.Instance.OnEchoesChanged -= UpdateStepsText;
    }

    private void UpdateStepsText(int steps)
    {
        // Update the TMP text
        if (stepsText != null)
        {
            stepsText.text = $"Steps: {steps}";
        }
    }
}