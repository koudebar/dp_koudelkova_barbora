/*-----------------------------------------
 * \file: AC2Updater.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Handles the AC2 task sequences, including target activation, ball throwing trials, and counting successful throws, while managing trial resets and transitions.
 -----------------------------------------
*/

using System;
using DeepSpace.LaserTracking;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Analytics;

/*
 * This script launches the sequences tied to AC2 tasks
 */
public class AC2Updater : MonoBehaviour
{
    public GameObject target;
    public GameObject accomplished; // Launch accomplished sequence
    public GameObject stopMeasurement;
    public GameObject startPlace;
    public GameObject stepZone;
    public GameObject wrongBallZone;
    public int numTrials = 10; // Number of trials a child has before scene change
    public bool debug = false;

    private bool isStandingDetected = false; // True if person stepped inside measurement area
    private bool stopMeasurementFlag = false; // True if measurement should stop
    private int trialCount = 0;
    private int successfulTrials = 0; // Number of trials where child threw the ball and did not step away
    private bool isBallPickedUp; // Child must come for its ball, otherwise it cannot shoot
    private bool pharusFault;
    private bool finished;
    private GameObject currentModel; // Reference to the current spawned model of the planet
    private GameObject currentExplosion; // Reference to the current explosion effect of the planet

    private DetectIfInShape detector;
    private DetectIfInShape ballInAreaDetector;
    private DetectIfInShape wronglyThrownArea;

    private void Start()
    {
        // Find the DetectIfInShape component
        detector = startPlace.GetComponent<DetectIfInShape>();
        ballInAreaDetector = stepZone.GetComponent<DetectIfInShape>();
        wronglyThrownArea = wrongBallZone.GetComponent<DetectIfInShape>();
    }

    private void Update()
    {
        ComePickUpTheBall();
        HandleStandingOnPlatform();
        HandleDetectionAndTrials();
    }

    // Handle detection and setup when standing on the platform
    private void HandleStandingOnPlatform()
    {
       // Debug.Log($"ball picked:{isBallPickedUp}, trialCount:{trialCount}, standingDetected:{isStandingDetected}, is StandingONPlatform:{GameManager.Instance.isStandingOnPlatform}");
       // Debug.Log($"ball detected:{GameManager.Instance.wasDetectedBall}, object was thrown:{GameManager.Instance.objectWasThrown}");
        if (!isBallPickedUp && trialCount >=1) return; //On first throw
        if (GameManager.Instance.isStandingOnPlatform && !isStandingDetected)
        {
            Debug.Log(" = ");
            Debug.Log($"Specific entity in area? {detector.IsSpecificEntityInArea(GameManager.Instance.standingEntity)}");
            GameManager.Instance.wasDetectedBall = false;
            GameManager.Instance.objectWasThrown = false;
            target.SetActive(true);
            SpawnPlanetSafely();
            stepZone.SetActive(true);
            wrongBallZone.SetActive(true);
            stopMeasurementFlag = false;
            accomplished.SetActive(false);
            stopMeasurement.SetActive(false);
            isBallPickedUp = false;
            finished = false;
            if (trialCount == 0)
            {
                target.transform.Find("tutorial").gameObject.SetActive(true);
                if(GameManager.Instance.oralExplanation)target.transform.Find("tutorial").transform.Find("ExplainOrally").gameObject.SetActive(true);
            }
            else
            {
                target.transform.Find("tutorial").gameObject.SetActive(false);
            }

            isStandingDetected = true;
        }
    }

    // Handle ball throwing and trial counting
    private void HandleDetectionAndTrials()
    {
        if (!isStandingDetected) return;

        // child stepped out from mat
        if (!detector.IsSpecificEntityInArea(GameManager.Instance.standingEntity) && !stopMeasurementFlag && !debug)
        {
            isBallPickedUp = true; //the child did not throw the ball we presume
            LogMeasurementStop("Child stepped from mat.", true);
        }
        // child accomplished throw
        else if (GameManager.Instance.objectWasThrown && !stopMeasurementFlag)
        {
            pharusFault = true;
            finished = true;
            LogSuccessfulThrow();
        }
        //child missed the planet
        else if (GameManager.Instance.wasDetectedBall && !GameManager.Instance.objectWasThrown && !finished)
        {
            pharusFault = true;
            stepZone.transform.Find("WrongBallSound").GetComponent<AudioSource>().Play();
            if(GameManager.Instance.oralExplanation)stepZone.transform.Find("WrongCommentary").GetComponent<AudioSource>().Play();
            LogMeasurementStop("Child did not land ball on the target.", false);
        }
    }

    // Log and handle measurement stop scenarios
    private void LogMeasurementStop(string message, bool trigger)
    {
        if (trigger)
        {
            stopMeasurement.SetActive(true);
            if(GameManager.Instance.oralExplanation)stopMeasurement.transform.Find("Commentary").GetComponent<AudioSource>().Play();
        }
        LogManager.Instance.LogMessage($"Stopped. {message}");
        LogManager.Instance.AC2[trialCount] = 0;
        CountTrials();
    }
    
    // Child threw the ball and is coming for it 
    private void ComePickUpTheBall()
    {
         if(ballInAreaDetector.entitiesInsideArea.Count >=2)
        {
            isBallPickedUp = true;
            GameManager.Instance.isStandingOnPlatform = false;
            pharusFault = false;
        }else if (pharusFault && ballInAreaDetector.IsSpecificEntityInArea(GameManager.Instance.standingEntity))
         {
             isBallPickedUp = true;
             GameManager.Instance.isStandingOnPlatform = false;
             pharusFault = false;
         }
    }

    // Log a successful throw
    private void LogSuccessfulThrow()
    {
        accomplished.SetActive(true);
        target.SetActive(false);
        successfulTrials++;
        currentExplosion = TriggerExplosionSafely();
        if(GameManager.Instance.oralExplanation)accomplished.transform.Find("GoodCommentary").GetComponent<AudioSource>().Play();

        DestroyModelAndExplosion();
        LogManager.Instance.LogMessage("Ball landed correctly.");
        LogManager.Instance.AC2[trialCount] = 1;
        CountTrials();
    }

    // Count the number of trials and switch scenes if necessary
    private void CountTrials()
    {
        trialCount++;
        Debug.Log($"Num of trial: {trialCount}");
        if (trialCount >= numTrials)
        {
            SwitchToNextScene();
        }
        ResetToDefault();
    }

    // Switch to the next scene when trials are complete
    private void SwitchToNextScene()
    {
        startPlace.SetActive(false);
        target.SetActive(false);
        wrongBallZone.SetActive(false);
        stopMeasurement.SetActive(false);
        LogManager.Instance.LogMessage("Ten trials done. Setting to different scene.");
    }

    // Reset game state to default for a new trial
    private void ResetToDefault()
    {
        if (trialCount >= 3)
        {
            accomplished.transform.Find("Animation").gameObject.SetActive(false);
        }

        GameManager.Instance.objectWasThrown = false;
        //GameManager.Instance.standingEntity = null;
        GameManager.Instance.isStandingOnPlatform = false;
        GameManager.Instance.wasDetectedBall = false;

        isStandingDetected = false;
        stopMeasurementFlag = true;

        target.SetActive(false);
        wrongBallZone.SetActive(false);
    }

    // Destroy the current model and explosion effect safely
    private void DestroyModelAndExplosion()
    {
        if (currentModel != null)
        {
            Destroy(currentModel);
            currentModel = null;
        }

        if (currentExplosion != null)
        {
            Destroy(currentExplosion, 5f); // Delay to allow explosion to finish playing
            currentExplosion = null;
        }
    }

    // Spawn a new planet model safely
    private GameObject SpawnPlanetSafely()
    {
        if (currentModel == null)
        {
            currentModel = SpawnManager.Instance.SpawnRandomPlanet();
        }
        return currentModel;
    }

    // Trigger an explosion effect safely
    private GameObject TriggerExplosionSafely()
    {
        if (currentExplosion == null)
        {
            currentExplosion = SpawnManager.Instance.TriggerExplosion();
        }
        return currentExplosion;
    }
    

}
