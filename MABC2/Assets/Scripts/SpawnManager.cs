/*-----------------------------------------
 * \file: SpawnManager.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: April 2024
 * \brief: Spawn random planet from array of prefabs and trigger random explosion in AC2 to a hardcoded position.
 -----------------------------------------
*/

using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance { get; private set; }

    public GameObject[] models; // Array of model prefabs
    public GameObject[] particleEffects; // Array of particle effects

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public GameObject SpawnRandomPlanet()
    {
        // Select a random model and a random particle effect
        GameObject modelPrefab = models[Random.Range(0, models.Length)];
        Vector3 spawnPosition = new Vector3(388,616,4074); // Example spawn position
        GameObject model = Instantiate(modelPrefab, spawnPosition, Quaternion.identity);
        return model;
    }

    public GameObject TriggerExplosion()
    {
        // Assume explosion occurs at a fixed position for simplicity, adjust as needed
        GameObject explosionEffect = particleEffects[Random.Range(0, particleEffects.Length)];
        GameObject effectInstance = Instantiate(explosionEffect, new Vector3(388,616,4074), Quaternion.identity); // Example explosion position
        return effectInstance;
    }
    
}