/*-----------------------------------------
 * \file: GenerateWarningSigns.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Handles the visualization of warning signs from BAL2 based on tracked entities' positions, 
 *         using a specified sprite prefab and tracking manager.
 -----------------------------------------
*/
using DeepSpace.LaserTracking;
using UnityEngine;

public class GenerateWarningSigns : MonoBehaviour
{
    public GameObject spritePrefab; // Prefab for the sprite to instantiate
    public TrackingEntityManager trackingManager; // Reference to the manager handling tracking

    private GameObject instantiatedSprite; // To hold the instantiated sprite

    void Update()
    {
        UpdateEntityVisualization();
    }
    // If entity is in area, instantiate the prefab and move it alongside entity
    private void UpdateEntityVisualization()
    {
        foreach (var entity in trackingManager.TrackingEntityList)
        {
            if (IsEntityInsideUI(entity))
            {
                if (instantiatedSprite == null)
                {
                    instantiatedSprite = Instantiate(spritePrefab, entity.transform.position, Quaternion.identity);
                }
                instantiatedSprite.transform.position = entity.transform.position;
                return; // Assumes only one entity will be tracked at a time
            }
        }

        // If no entity is inside, destroy the sprite
        if (instantiatedSprite != null)
        {
            Destroy(instantiatedSprite);
            instantiatedSprite = null;
        }
    }

    private bool IsEntityInsideUI(TrackingEntity entity)
    {
        // Get the RectTransform component of the image that represents the area
        RectTransform areaRectTransform = GetComponent<RectTransform>();

        // Since we are in World Space, we'll use the corners of the RectTransform to create the bounding box
        Vector3[] worldCorners = new Vector3[4];
        areaRectTransform.GetWorldCorners(worldCorners); // Bottom left, top left, top right, bottom right

        // Log the corners of the rectangle
        //Debug.Log(
        // $"World Space Rectangle Corners: BL {worldCorners[0].ToString()}, TL {worldCorners[1].ToString()}, TR {worldCorners[2].ToString()}, BR {worldCorners[3].ToString()}");

        // Create a Rect from the RectTransform's world corners
        Vector2 bottomLeft = worldCorners[0];
        Vector2 topRight = worldCorners[2];
        Rect worldSpaceRect =
            new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);

        // Log the constructed rectangle
        //Debug.Log($"World Space Rectangle: {worldSpaceRect.ToString()}");

        // Get the world position of the entity
        Vector3 entityWorldPosition = entity.transform.position;

        // Log the entity's world position
        //Debug.Log($"Entity {entity.TrackID.ToString()} World Position: {entityWorldPosition.ToString()}");

        // Check if the entity's world position is within the Rect bounds
        // Note: The entity's z-coordinate is not considered in the 2D Rect.
        bool inside = worldSpaceRect.Contains(new Vector2(entityWorldPosition.x, entityWorldPosition.y));

        // Log the result of the containment check
        //Debug.Log($"Is Entity {entity.TrackID.ToString()} inside the area? {inside}");

        return inside;
    }
}
