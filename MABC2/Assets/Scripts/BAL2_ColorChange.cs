/*-----------------------------------------
 * \file: BAL2_ColorChange.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Changes the color of image to indicate proper walking in BAL2.
 -----------------------------------------
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BAL2_ColorChange : MonoBehaviour
{
    public Image targetSprite;
    private int colorIndicator;

    private void Awake()
    { 
        targetSprite = GetComponent<Image>();
    }

    void Update()
    {
        colorIndicator = GameManager.Instance.walkingCorrectly;

        switch (colorIndicator)
        {
            case 1:
                targetSprite.color = new Color(0f, 1f, 0f, 0.6f); //green
                break;
            case 0:
                targetSprite.color =  new Color(1f, 0f, 1f, 0.6f); // Magenta
                break;
            default:
                targetSprite.color = new Color(0.8f, 0.8f, 0.8f, 0.6f); // Gray
                break;
        }
    }
}
