/*-----------------------------------------
 * \file: DelayedAnimationStart.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Plays a specified animation clip after a configurable delay, and allows manual triggering of the animation.
 -----------------------------------------
*/

using UnityEngine;

public class DelayedAnimationStart : MonoBehaviour
{
    public float delayInSeconds = 1.0f; 
    public string nameOfAnimationClip = "";
    void Start()
    {
        Animation animation = GetComponent<Animation>();
        if (animation != null)
        {
            animation[nameOfAnimationClip].enabled = false; 
            Invoke(nameof(PlayAnimation), delayInSeconds); // Schedule the animation to start after the delay
        }
    }

    public void PlayAnimation()
    {
        Animation animation = GetComponent<Animation>();
        if (animation != null)
        {
            animation[nameOfAnimationClip].enabled = true; // Re-enable the animation clip
            animation.Play(nameOfAnimationClip); // Play the specified animation
        }
    }
}