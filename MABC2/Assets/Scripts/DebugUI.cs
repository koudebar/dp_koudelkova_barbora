/*-----------------------------------------
 * \file: DebugUI.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Debug purposes only. Detect if pointer is hovering over and clicked to ui.
 -----------------------------------------
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DebugUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(0)) {  // Checks for the first mouse button click
            if (EventSystem.current.IsPointerOverGameObject())
                Debug.Log("Click on UI element.");
            else
                Debug.Log("Click not on UI element.");
        }
    }
}
