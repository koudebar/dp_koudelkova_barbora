/*-----------------------------------------
 * \file: DetectedAreaResolve.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Resolves the player's interaction with each mat tile based on echoes and entity positions, updating the game state accordingly. Note that the ui element needs to has
 *         both DetectedAreaResolve and DetectIfInShape to ensure proper work.
 -----------------------------------------
*/

using System.Collections;
using System.Collections.Generic;
using DeepSpace.LaserTracking;
using JetBrains.Annotations;
using NUnit.Framework.Constraints;
using UnityEngine;

/*This script is used for each tile player is expected to stand on
 * in inspector, set the desired behaviour on what the interaction with tile should be
 * and this script chooses what it will do with that information
 * */
public class DetectedAreaResolve : MonoBehaviour
{

    public enum typeOfInteraction
    {
        PlayerStandingWholeTime, //standing on platform for whole time
        BallThrown, //detected ball on position
        PlayerStandingOnStart, //standing on start position
        FeetOnLine, //echoes placed on line
        FeetOffLine, //echoes detected in the wrong line
        PlayerInsideArea, //someone is occupying space
        PlayerFinishedWalking, //launch stop sequence
        BallInsideArea,
        TherapistCircle, // Therapist standing in circle
        AC1_not_OK, //incorrect catch
        AC1_OK // correct catch
    }

    public typeOfInteraction interaction;
    private DetectIfInShape detector;
    public bool printAllDetails = false;
    public bool printEchoes = false;
    public bool visualiseEchoes = false;
    
    [CanBeNull]public GameObject echoPrefab; // Prefab for visualizing echoes
    private Canvas canvas;
    private bool firstEchoDetected = false;
    private bool firstEntityDetected = false;
    private bool secondEntityDetected = false;
    private TrackingEntity firstEntity;

    private bool problemWithNull; //detector list EntitiesInsideArea can be full of missing references. This was found at the very end of submission.
    

    private void Start()
    {
        // Find the DetectIfInShape component on the GameObject this script is attached to
        detector = GetComponent<DetectIfInShape>();
        // This finds a Canvas object in the current loaded scene for echo visualisation
        canvas = FindObjectOfType<Canvas>();
    }

    private void Update()
    {
        problemWithNull = true;
        /*IS ENTITY INSIDE OF ME?*/
        if (detector != null && detector.entitiesInsideArea.Count > 0)
        {
            foreach (var entity in detector.entitiesInsideArea)
            {
                if (entity != null) problemWithNull = false;
                if (printAllDetails)
                {
                    // Print detailed information about each entity
                    Debug.Log($"Entity ID: {entity.TrackID}, " +
                              $"Abs Pos: {entity.AbsolutePosition}, " +
                              $"Next Expected Abs Pos: {entity.NextExpectedAbsolutePosition}, " +
                              $"Rel Pos: {entity.RelativePosition}, " +
                              $"Orientation: {entity.Orientation}, " +
                              $"Speed: {entity.Speed} m/s");

                    // Print information about echoes if available
                    if (entity.Echoes.Count > 0)
                    {
                        for (int i = 0; i < entity.Echoes.Count; i++)
                        {
                            if (printAllDetails)
                                Debug.Log($"Echo {i + 1}: x:{entity.Echoes[i].x}, y:{entity.Echoes[i].y}");
                        }

                    }
                    else
                    {
                        if (printAllDetails) Debug.Log("No echoes detected for this entity.");
                    }
                }
                
                //Remember only the first echo position, then remember the second(Ball problems)
                if (!firstEntityDetected)
                {
                    firstEntity = entity;
                    GameManager.Instance.xyCoordsOfDetection = entity.transform.position; //obtain it in world coords
                    firstEntityDetected = true;
                }else if(firstEntity && !secondEntityDetected && entity.TrackID != firstEntity.TrackID)
                {
                    secondEntityDetected = true;
                }

            }
            
            if(!problemWithNull)updateInteraction(interaction);
        }
        else
        {
            firstEntityDetected = false;
            secondEntityDetected = false;
            firstEntity = null;
        }

        /*IS ECHO INSIDE OF ME?*/
        if (detector != null && detector.echoesInsideArea.Count > 0)
        {
            foreach (Vector2 coordinates in detector.echoesInsideArea)
            {
                if (printAllDetails || printEchoes)
                {
                    Debug.Log($"Echo: x:{coordinates[0]}, y:{coordinates[1]}");
                }
                
                //Remember only the first echo position
                if (!firstEchoDetected)
                {
                    GameManager.Instance.xyCoordsOfEchoDetection = coordinates;
                    firstEchoDetected = true;
                }
            }
            if(visualiseEchoes)VisualizeEchoes(detector.echoesInsideArea);
            updateInteractionBasedOnEcho(interaction);
        }

    }
    
    // create sprite that lasts on screen of detected echo
    private void VisualizeEchoes(List<Vector2> echoes)
    { 
        foreach (Vector2 echo in echoes)
        {
            if (printAllDetails || printEchoes)
            {
                Debug.Log($"Echo: x:{echo.x}, y:{echo.y}");
            }
            if (echoPrefab != null && canvas != null)
            {
                GameObject echoVisual = Instantiate(echoPrefab, new Vector3(echo.x, echo.y, 0), Quaternion.identity) as GameObject;
                SpriteRenderer echoImage = echoVisual.GetComponent<SpriteRenderer>();
                if (echoImage != null)
                {
                    echoImage.color = new Color(0.5f, 0f, 0.5f, 1f);  // RGBA for purple
                }
            }
        }
    }

    public void updateInteraction(typeOfInteraction interaction)
    {
        switch (interaction)
        {
            case typeOfInteraction.PlayerStandingWholeTime:
                GameManager.Instance.isStandingOnPlatform = true;
                GameManager.Instance.standingEntity = firstEntity;
                break;
            case typeOfInteraction.BallThrown:
                GameManager.Instance.objectWasThrown = true;
                GameManager.Instance.xyCoordsOfBallThrow = GameManager.Instance.xyCoordsOfEchoDetection;
                break;
            case typeOfInteraction.PlayerStandingOnStart:
                GameManager.Instance.isStandingOnStart = true;
                GameManager.Instance.standingEntity = firstEntity; //for audio
                break;
            case typeOfInteraction.PlayerFinishedWalking:
                GameManager.Instance.finishedWalking = true;
                GameManager.Instance.standingEntity = firstEntity;
                break;
            case typeOfInteraction.PlayerInsideArea:
                GameManager.Instance.wasDetectedBall = true;
                if (secondEntityDetected)
                {
                    GameManager.Instance.wasDetectedSecondObject = true;
                    secondEntityDetected = false;
                }
                break;
            case typeOfInteraction.BallInsideArea:
                break;
            case typeOfInteraction.TherapistCircle:
                GameManager.Instance.therapistInCircle = true;
                break;
            case typeOfInteraction.AC1_not_OK:
                GameManager.Instance.AC1_incorrectCatch = true;
                break;
            case typeOfInteraction.AC1_OK:
                GameManager.Instance.AC1_correctCatch = true;
                break;
            default:
                break;
        }
    }
    
    public void updateInteractionBasedOnEcho(typeOfInteraction interaction)
    {
        switch (interaction)
        {
            case typeOfInteraction.FeetOnLine:
                GameManager.Instance.walkingCorrectly = 1;
                break;
            case typeOfInteraction.FeetOffLine:
                GameManager.Instance.walkingCorrectly = 0;
                break;

            default:
                break;
        }
    }

}