/*-----------------------------------------
 * \file: StartPythonScript.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Run python script on separate thread. For working purpose only, not recommended for build purposes, since the thread needs to kill itself as well. Used for mediapipe detection.
 -----------------------------------------
*/


using System.Diagnostics;
using System.Threading;
using UnityEngine;

public class StartPythonScript : MonoBehaviour
{
    private Thread pythonThread;
    public bool stopRequested = false;

    void Start()
    {
        // Start the Python script in a separate thread
        pythonThread = new Thread(new ThreadStart(RunPythonScript));
        pythonThread.Start();
    }

    void Update()
    {
        // Check for a condition to stop the Python script
        if (stopRequested)
        {
            StopPythonScript();
        }
    }

    private void RunPythonScript()
    {
        using (Process pythonProcess = new Process())
        {
            pythonProcess.StartInfo.FileName = "E:\\FEL\\DP\\mediapipe\\venv\\Scripts\\python.exe";
            pythonProcess.StartInfo.Arguments = "E:\\FEL\\DP\\mediapipe\\AnkleDiff.py";
            pythonProcess.StartInfo.UseShellExecute = false;
            pythonProcess.StartInfo.RedirectStandardOutput = true;
            pythonProcess.StartInfo.RedirectStandardError = true;


            try
            {
                pythonProcess.Start();
                string stderr = pythonProcess.StandardError.ReadToEnd();
                if (!string.IsNullOrEmpty(stderr))
                {
                    UnityEngine.Debug.Log("Python Error: " + stderr);
                }
                while (!pythonProcess.StandardOutput.EndOfStream)
                {
                    string line = pythonProcess.StandardOutput.ReadLine();
                    UnityEngine.Debug.Log(line);

                    // Check frequently if a stop has been requested
                    if (stopRequested)
                    {
                        pythonProcess.Kill();
                        break;
                    }
                }
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError("Failed to run python script: " + e.Message);
            }
        }
    }

    public void StopPythonScript()
    {
        if (pythonThread != null && pythonThread.IsAlive)
        {
            stopRequested = true; // Signal the thread to stop
            pythonThread.Join(); // Wait for the thread to finish
        }
    }

    void OnDestroy()
    {
        // Ensure the Python script is stopped when the GameObject is destroyed
        StopPythonScript();
    }
}