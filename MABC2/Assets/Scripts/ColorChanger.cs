/*-----------------------------------------
 * \file: ColorChanger.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: February 2024
 * \brief: Change color of object in time. Not currently used.
 -----------------------------------------
*/

using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    public Color[] colors; // Array to hold your palette colors
    public float changeInterval = 2f; // Duration of each color transition

    private Material material; // Material to change colors
    private int currentColorIndex = 0; // Index for the current color
    private float transitionStartTime; // Time when the current transition started
    public float startDelay = 0f; // Delay before the color transitions start

    void Start()
    {
        if (colors.Length == 0) return; // Exit if no colors are defined

        material = GetComponent<Renderer>().material; // Get the material of the GameObject
        material.color = colors[currentColorIndex]; // Set the initial color
        transitionStartTime = Time.time + startDelay; // Initialize the start time of the transition
    }

    void Update()
    {
        if (colors.Length < 2) return; // Need at least two colors to transition between

        // Calculate how far through the current transition we are (from 0 to 1)
        float t = (Time.time - transitionStartTime) / changeInterval;

        // Ensure t never exceeds 1.0
        if (t >= 1.0f)
        {
            t = 0.0f;
            transitionStartTime = Time.time;

            // Update indices for the next transition
            currentColorIndex = (currentColorIndex + 1) % colors.Length;
        }

        // Determine the index of the next color
        int nextColorIndex = (currentColorIndex + 1) % colors.Length;

        // Smoothly transition to the next color, ensuring we cover the last step fully
        Color targetColor = Color.Lerp(colors[currentColorIndex], colors[nextColorIndex], t);
        material.color = targetColor;
    }
}