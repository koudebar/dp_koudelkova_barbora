/*-----------------------------------------
 * \file: UDPSceneCommunication.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: This script communicates via loopback with mediapipe python app, telling it the name of the scene and if it should record motion data.
 *              The app listens on port 5005.
 -----------------------------------------
*/

using System;
using System.Net.Sockets;
using System.Text;
using NUnit.Framework.Constraints;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UDPSceneCommunication : MonoBehaviour
{
    private const string ServerAddress = "127.0.0.1"; // Loopback address
    private const int Port = 5005; // Same port as the Python script
    private UdpClient udpClient;
    
    private bool recording_flag;

    void Start()
    {
        udpClient = new UdpClient();
        SceneManager.sceneLoaded += OnSceneLoaded;
        recording_flag = false;
    }

    // Basically event OnRecordingFlagChange to subscribe to
    private void Update()
    {
        if (GameManager.Instance.wasRaisedRecording && !recording_flag)
        {
            recording_flag = true;
            SendMessageToServer("RECORDING");
        }else if (!GameManager.Instance.wasRaisedRecording && recording_flag)
        {
            SendMessageToServer("RECORDING");
            recording_flag = false;
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SendMessageToServer(GameManager.Instance.SceneName);
    }
    
    public void SendMessageToServer(string message)
    {
        try
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            udpClient.Send(data, data.Length, ServerAddress, Port);
        }
        catch (SocketException e)
        {
            Debug.LogError($"Socket exception: {e.Message}");
        }
    }

    void OnDestroy()
    {
        udpClient?.Close();
    }
}