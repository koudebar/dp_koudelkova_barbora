/*-----------------------------------------
 * \file: LoopSoundInArea.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: April 2024
 * \brief: Start audio if entity stepped on ropevalking area BAL1, log the start and stop state.
 -----------------------------------------
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopSoundInArea : MonoBehaviour
{
    private AudioSource audioSource;
    private int peopleInArea = 0; // Count of people currently in the area

    private bool startPlaying = false;
    private DetectIfInShape detector;
    
    void Start()
    {
        // Get the AudioSource component attached to this GameObject
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            // Add an AudioSource if not already attached
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.loop = true; // Set the audio to loop
            audioSource.playOnAwake = false; // Prevent it from playing automatically
            detector = GetComponent<DetectIfInShape>();
        }
        
        // Ensure there's an AudioClip assigned in the Inspector or via script
        if (audioSource.clip == null)
        {
            Debug.LogError("No audio clip assigned to AudioSource!");
        }

        detector = GetComponent<DetectIfInShape>();
        if (detector == null)
        {
            Debug.LogError("DetectIfInShape component not found on the GameObject!");
        }
    }

    private void Update()
    {
        if (detector.entitiesInsideArea.Count > 0)
        {
            if (!startPlaying)
            {
                audioSource.Play();
                startPlaying = true;
               LogManager.Instance.LogMessage("Child started walking");
            }
        }
        else if (detector.entitiesInsideArea.Count == 0)
        {
            if (audioSource.isPlaying)
            {
                startPlaying = false;
                audioSource.Stop();
                LogManager.Instance.LogMessage("Child stopped walking");
            }
        }
    }
}
