﻿/*-----------------------------------------
 * \file: RandomRotator.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: April 2024
 * \brief: Rotate sphere in AC2 manually.
 -----------------------------------------
*/

using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour
{
    [SerializeField]
    private float tumble;

    void Start()
    {
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
    }
}