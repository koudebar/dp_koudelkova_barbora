/*-----------------------------------------
 * \file: LogManager.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: April 2024
 * \brief: Exports selected Unity debug logs and histograms to CSV files for analysis.
 -----------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using Debug = UnityEngine.Debug;

/*
 * This class is for exporting debug logs from console to csv
 * Attached to the LogicManager prefab, which is not destroyed on load.
 */
public class LogManager : MonoBehaviour
{
    private List<string[]> logEntries = new List<string[]>();
    public static LogManager Instance;
    public bool continueLoggingtoDebugConsole = true;
    private String dateTimeStamp;
    
    // Variables in format for Testing sheet
    public int[]  AC1;
    public int[]  AC2;
    public float BAL1_left;
    public float BAL1_right;
    public int BAL2steps;
    public int BAL3;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        AC1 = new int[10];
        AC2 = new int[10];
        BAL1_left = 0;
        BAL1_right = 0;
        BAL2steps = 0;
        BAL3 = 0;
    }

    public void LogMessage(string message)
    {
        // Set the data from scenemanager
        string sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        string timestamp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        logEntries.Add(new string[] { sceneName, timestamp, message });
        if(continueLoggingtoDebugConsole)Debug.Log(message);
    }

    // Child stopped walking in BAL1, export histogram to csv
    public void LogHistogram(int measureNumber, float[] echoHistogram, int[] echoRegisteredAsFootstep, int[] echoRegisteredAsHeelRaise)
    {
        string dateTimeStampNow = DateTime.Now.ToString("yyyy-MM-dd_HH-mm");
        string filePath = Application.persistentDataPath + $"/histogram_{dateTimeStampNow}_{measureNumber.ToString()}.csv";
        Debug.Log("Saving Histogram CSV to: " + filePath);

        using (StreamWriter file = new StreamWriter(filePath))
        {
            // Write header with measure number and current timestamp
            //string timestamp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //file.WriteLine($"Measure no. {measureNumber}, Date of log: {timestamp}");

            // Column headers for data
            file.WriteLine("Bin Index, Echo Histogram Value, Registered As Footstep, Registered As Heel Raise");
            
            int length = echoHistogram.Length;
            for (int i = 0; i < length; i++)
            {
                string line = $"{i}, {echoHistogram[i]}, {echoRegisteredAsFootstep[i]}, {echoRegisteredAsHeelRaise[i]}";
                file.WriteLine(line);
            }
        }

        if (continueLoggingtoDebugConsole)
            Debug.Log($"Logged histogram to CSV: Measure no. {measureNumber}");
    }

    // Unity app is terminated, save the Debug Logs to persistent paths, and open the saved logs
    void OnApplicationQuit()
    {
        dateTimeStamp = DateTime.Now.ToString("yyyy-MM-dd_HH-mm");
        ExportLogsToCSV();
        ExportTestingDataToCSV();
        OpenCSV();
    }

    private void ExportLogsToCSV()
    {
        // Include the current day, minute, and hour in the filename
        string filePath = Application.persistentDataPath + $"/echo_logs_{dateTimeStamp}.csv";
        Debug.Log("Saving CSV to: " + filePath);
        using (StreamWriter file = new StreamWriter(filePath))
        {
            file.WriteLine("Scene Name, Timestamp, Message"); // CSV Header
            foreach (var entry in logEntries)
            {
                file.WriteLine(string.Join(",", entry));
            }
        }
    }

    // Open explorer to where the csv are saved.
    private void OpenCSV()
    {
        // Include the current day, minute, and hour in the filename
        string filePath = Application.persistentDataPath + $"/echo_logs_{dateTimeStamp}.csv";
        string argument = "/select, \"" + filePath.Replace("/", "\\") + "\"";

        try
        {
            Process.Start("explorer.exe", argument);
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Failed to open CSV file: " + ex.Message);
        }
    }
    
    // For counting the hard score for therapist
    private void ExportTestingDataToCSV()
    {
        // Include the current day, minute, and hour in the filename
        string filePath = Application.persistentDataPath + $"/testing_score_{dateTimeStamp}.csv";
        Debug.Log("Saving Testing Data CSV to: " + filePath);

        using (StreamWriter file = new StreamWriter(filePath))
        {
            // Headers
            file.WriteLine("Exercise,Value");

            // Export array data
            file.WriteLine($"AC1 Count,{SumArray(AC1)}");
            for (int i = 0; i < AC1.Length; i++)
            {
                file.WriteLine($"AC1[{i}],{AC1[i]}");
            }

            file.WriteLine($"AC2 Count,{SumArray(AC2)}");
            for (int i = 0; i < AC2.Length; i++)
            {
                file.WriteLine($"AC2[{i}],{AC2[i]}");
            }

            // Export single float and int values
            file.WriteLine($"BAL1_left,{BAL1_left}");
            file.WriteLine($"BAL1_right,{BAL1_right}");
            file.WriteLine($"BAL2steps,{BAL2steps}");
            file.WriteLine($"BAL3,{BAL3}");
        }
    }
    
    
    // Helper method to calculate the sum of an integer array
    private int SumArray(int[] array)
    {
        int sum = 0;
        foreach (int value in array)    
        {
            sum += value;
        }
        return sum;
    }
}