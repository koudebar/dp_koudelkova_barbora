/*-----------------------------------------
 * \file: InterfaceClicker.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: March 2024
 * \brief: Manages the interface for controlling various gameplay features via UI button interaction.
 *         Handles the connection status of tablets and modifies the UI accordingly.
 -----------------------------------------
*/

using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceClicker : MonoBehaviour
{
    [CanBeNull]public Button hostButton;
    [CanBeNull]public Button tabletConnected;
    [CanBeNull]public Button recordingButton;
    [CanBeNull]public Button explanationButton;
    [CanBeNull]public GameObject tabletText;
  
    private void Start()
    {
        //Disable the ability to host a session if you are a handheld
        if (GameManager.Instance.type == "Handheld")
        {
            // Disable the button
            if (hostButton != null)
            {
                hostButton.interactable = false;
                ColorBlock cb = hostButton.colors;
                cb.disabledColor = Color.grey;
                hostButton.colors = cb; 
            }
        }
        
        // Hide scene change menu for scene changes, if tablet is connected
        if (GameManager.Instance.isOperatorOverride)
        {
            // Find UI_Menu_Change GameObject in the scene
            GameObject menuChangeObject = GameObject.Find("UI_Menu_Change");

            // Check if UI_Menu_Change GameObject exists
            if (menuChangeObject != null)
            {
                // Set UI_Menu_Change to active
                    menuChangeObject.SetActive(false);
            }
            else
            {
                Debug.Log("cannot find object");
            }

        }
        // Since script EchoHoppingTracker is for reasons NOT initialized at the very beginning of runtime, this thing does the trick
        // For UI without tablet connected
        if (GameManager.Instance.wasRaisedRecording && recordingButton != null)
        {

            ColorBlock cb = recordingButton.colors;
            // Set to green
            cb.normalColor = Color.green;
            cb.selectedColor = Color.green;
            cb.pressedColor = Color.green;
            Debug.Log("Recording has started.");
            // Apply the changes to the button
            recordingButton.colors = cb;
        }
        
        if (GameManager.Instance.oralExplanation && explanationButton != null)
        {

            ColorBlock cb = explanationButton.colors;
            // Set to green
            cb.normalColor = Color.yellow;
            cb.selectedColor = Color.yellow;
            cb.pressedColor = Color.yellow;
            Debug.Log("Explanation is on.");
            // Apply the changes to the button
            explanationButton.colors = cb;
        }
        
    }

    private void Update()
    {
        if (!GameManager.Instance.isOperatorOverride) //tablet not connected, proceed without it and set the menu for every scene
        {
            if (tabletConnected != null)
            {
                tabletConnected.interactable = false;
                ColorBlock cb = tabletConnected.colors;
                cb.disabledColor = Color.grey;
                tabletConnected.colors = cb; 
            }
        }
        else if(GameManager.Instance.isOperatorOverride){
            
            //Finding AGAIN the menu is because of BAL3 script execution order, which is loaded before the start
            GameObject menuChangeObject = GameObject.Find("UI_Menu_Change");
            if (menuChangeObject != null)
            {
                menuChangeObject.SetActive(false);
            }
            
            if (tabletText != null)
            {
                tabletText.SetActive(true);
                tabletConnected.interactable = true;
                ColorBlock cb = tabletConnected.colors;
                cb.normalColor= Color.green;
                tabletConnected.colors = cb; 
            }
        }
    }

    // App is controlled via tablet, this is the tablet
    public void Operator()
    {
        GameManager.Instance.isOperator = 1;

    }

    public void Host()
    {
        GameManager.Instance.isOperator = 0;
    }

    //Load default scene, but if it does have operator, it will  listen to it and change scenes accordingly
    public void SessionActive()
    {
        if (GameManager.Instance.isOperatorOverride)
        {
            GameManager.Instance.SceneName = "AIMCATCH1";
            SceneManager.LoadScene("AIMCATCH1");
        }
        else
        {
            GameManager.Instance.SceneName = "AIMCATCH1";
            SceneManager.LoadScene("AIMCATCH1");
        }
    }

    //Set scene name for broadcasting to host
    public void SetSceneName(string sceneName)
    {
        GameManager.Instance.SceneName = sceneName;
        Debug.Log($"Scene name set to: {GameManager.Instance.SceneName}");
    }

    //Change scene, if not controlled by tablet
    public void ChangeScene(string sceneName)
    {
        // Check if the requested scene is the current scene
        if (SceneManager.GetActiveScene().name == sceneName)
        {
            // Reload the same scene
            GameManager.Instance.SceneName = sceneName;
            SceneManager.LoadScene(sceneName);
        }
        else
        {
            // Load a different scene
            GameManager.Instance.SceneName = sceneName;
            SceneManager.LoadScene(sceneName);
            

        }
    }

    // Recording  button pressed
    public void SetRecording()
    {

        // Toggle the state
        GameManager.Instance.wasRaisedRecording = !GameManager.Instance.wasRaisedRecording;

        // Fetch current button colors
        ColorBlock cb = recordingButton.colors;

        if (GameManager.Instance.wasRaisedRecording)
        {
            // Set to green
            cb.normalColor = Color.green;
            cb.selectedColor = Color.green;
            cb.pressedColor = Color.green;
            Debug.Log("Recording has started.");
        }
        else
        {
            // Set to gray
            cb.normalColor = Color.white;
            cb.selectedColor = Color.white;
            cb.pressedColor = Color.white;
            Debug.Log("Recording has stopped.");
        }

        // Apply the changes to the button
        recordingButton.colors = cb;
    }
    
    // Oral explanation  button pressed
    public void SetCommentary()
    {
        // Toggle the state
        GameManager.Instance.oralExplanation = !GameManager.Instance.oralExplanation;

        // Fetch current button colors
        ColorBlock cb = explanationButton.colors;

        if (GameManager.Instance.oralExplanation)
        {
            // Set to yellow
            cb.normalColor = Color.yellow;
            cb.selectedColor = Color.yellow;
            cb.pressedColor = Color.yellow;
            Debug.Log("Explanation has started.");
        }
        else
        {
            // Set to gray
            cb.normalColor = Color.white;
            cb.selectedColor = Color.white;
            cb.pressedColor = Color.white;
            Debug.Log("Explanaition has stopped.");
        }

        // Apply the changes to the button
        explanationButton.colors = cb;
    }

    // End application
    public void Quit()
    {
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
