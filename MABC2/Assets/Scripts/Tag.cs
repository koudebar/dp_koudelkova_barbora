/*-----------------------------------------
 * \file: Tag.cs
 * \author: Bc.Barbora Koudelkova
 * \school: Czech Technical University in Prague
 * \date: May 2024
 * \brief: Tags objects. Not currently used.
 -----------------------------------------
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tag : MonoBehaviour
{
    void Start()
    {
        //Set the tag of this GameObject
        gameObject.tag = "ActivateMe";
    }

}
